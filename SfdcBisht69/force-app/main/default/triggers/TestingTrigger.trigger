trigger TestingTrigger on Account (after insert) {
    TestingHelper t = new TestingHelper();
     if(trigger.isafter && trigger.isinsert) // Using context variables.
    {
     t.fun(trigger.new); // Calling apex class method.
    }
}