trigger OpportunityOrderTrigger on Opportunity (after update) {
    OpportunityOrderTriggerHelper obj=new OpportunityOrderTriggerHelper(); 
    if(trigger.isafter && trigger.isupdate) 
    {
        system.debug('inside trigger');
        obj.createOrder(trigger.new); 
    }
}