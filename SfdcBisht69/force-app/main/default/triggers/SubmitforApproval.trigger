trigger SubmitforApproval on Opportunity (after insert,after update) {
    for(Opportunity opp:trigger.New){       
        try{
            if(Trigger.isInsert){
                if(opp.Approval_Step__c == 'Submit'){
                    ProcessApprovelRequest.submitForApproval(opp);
                }
                else if(opp.Approval_Step__c == 'Approve'){
                    ProcessApprovelRequest.approveRecord(opp);
                }
                else if(opp.Approval_Step__c == 'Reject'){
                    ProcessApprovelRequest.rejectRecord(opp);
                }
            }
            else if(Trigger.isUpdate){
                Opportunity oppOld=Trigger.OldMap.get(opp.Id);
                if( opp.Approval_Step__c == 'Submit' && oppOld.Approval_Step__c != 'Submit') {
                    ProcessApprovelRequest.submitForApproval(opp);
                }
                else if(opp.Approval_Step__c == 'Approve' && oppOld.Approval_Step__c != 'Approve'){
                    ProcessApprovelRequest.approveRecord(opp);
                }
                else if(opp.Approval_Step__c == 'Reject' && oppOld.Approval_Step__c != 'Reject'){
                    ProcessApprovelRequest.rejectRecord(opp);
                }
            }
        }
        catch(Exception e){
            opp.addError(e.getMessage());
        }
    }
}