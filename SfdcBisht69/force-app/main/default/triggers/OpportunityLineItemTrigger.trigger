trigger OpportunityLineItemTrigger on OpportunityLineItem (after insert, after update, after delete) {
	if(trigger.isAfter){
		if(trigger.isInsert || trigger.isUpdate ){
			OppLineItemTriggerHandler.getMinimumQuantity(trigger.new);
			
		}
		if(trigger.isDelete){
			OppLineItemTriggerHandler.getMinimumQuantity(trigger.old);
		}
	}
}