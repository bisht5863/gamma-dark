trigger StudentTrigger on Student__c (after insert,after update,before delete) {
    if(trigger.isafter && trigger.isinsert){
        StudentTriggerHandler.updateTotalSubjectRecord(trigger.new);
        System.debug('inside studenttrigger');
    }
    if(trigger.isafter && trigger.isupdate){
        StudentTriggerHandler.updateTotalSubjectRecord(trigger.new);
        System.debug('update inside studenttrigger');
    }
}