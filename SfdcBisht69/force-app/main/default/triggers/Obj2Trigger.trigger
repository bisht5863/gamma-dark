trigger Obj2Trigger on Obj2__c (after undelete) {
    if(trigger.isafter && trigger.isundelete) // Using context variable.
    {
     Obj2TriggerHandler.updateOwnerObj1(trigger.new); // Calling apex class method.
    }
}