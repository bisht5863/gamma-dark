import { LightningElement } from 'lwc';
import customerDetailsFunction from '@salesforce/apex/cardDetailClass.customerDetailsFunction';
export default class CardTokenComponent extends LightningElement {
    customerName;
    customerEmail;
    customerAddress;
    customerPhone;
    customerCardNumber;
    customerCardExpireDate;
    customerCardCVC;
    card1Flag = true;
    card2Flag = false;
    allChanges(event) {
        if (event.target.name === 'CustomerName') {
            this.customerName = event.target.value;
            console.log('customer name-->' + this.customerName);
        }
        else if (event.target.name === 'Email') {
            this.customerEmail = event.target.value;
            console.log('customerEmail name-->' + this.customerEmail);
        }
        else if (event.target.name === 'CustomerCardNumber') {
            this.customerCardNumber = event.target.value;
            console.log('customerCardNumber-->' + this.customerCardNumber);
        }
        else if (event.target.name === 'expireDate') {
            this.customerCardExpireDate = event.target.value;
            console.log('customerCardExpireDate-->' + this.customerCardExpireDate);
        }
        else if (event.target.name === 'cardCVC') {
            this.customerCardCVC = event.target.value;
            console.log('customerCardCVC-->' + this.customerCardCVC);
        }
        else {
            this.customerPhone = event.target.value;
            console.log('customerPhone name-->' + this.customerPhone);
        }
    }
    submitClick() {
        this.card1Flag = false;
        this.card2Flag = true;
    }
    submitCardClick() {
        var customerDetails = {
            'customerName': this.customerName,
            'customerEmail': this.customerEmail,
            'customerPhone': this.customerPhone
        }
        var expDate = this.customerCardExpireDate.split("-", 3);
        console.log('datearray->' + expDate[0] + ' ' + expDate[1]);
        let cardDetailsvariable = [this.customerCardNumber, expDate[0], expDate[1], this.customerCardCVC];
        customerDetailsFunction({
            customerDetails: JSON.stringify(customerDetails),
            cardDetailsvariable: cardDetailsvariable
        })
    }

}