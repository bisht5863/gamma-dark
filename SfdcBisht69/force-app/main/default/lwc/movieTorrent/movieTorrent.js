import { LightningElement, track } from 'lwc';
import fun from '@salesforce/apex/YtsClass.fun';
import searchMovieFun from '@salesforce/apex/YtsClass.searchMovieFun';
import clickGenreMovie from '@salesforce/apex/YtsClass.clickGenreMovie';
import fun1 from '@salesforce/apex/Yts3dClass.fun1';
import torrentLOGO from '@salesforce/resourceUrl/torrentLOGO';
import actionLOGO from '@salesforce/resourceUrl/actionLOGO';
import comedyLOGO from '@salesforce/resourceUrl/comedyLOGO';
import thrillerLOGO from '@salesforce/resourceUrl/thrillerLOGO';
import crimeLOGO from '@salesforce/resourceUrl/crimeLOGO';
import biographyLOGO from '@salesforce/resourceUrl/biographyLOGO';
import dramaLOGO from '@salesforce/resourceUrl/dramaLOGO';
import horrorLOGO from '@salesforce/resourceUrl/horrorLOGO';
import scifiLOGO from '@salesforce/resourceUrl/scifiLOGO';
import adventureLOGO from '@salesforce/resourceUrl/adventureLOGO';
import romanceLOGO from '@salesforce/resourceUrl/romanceLOGO';
import imbdLOGO from '@salesforce/resourceUrl/imbdLOGO';
import fantasyLOGO from '@salesforce/resourceUrl/fantasyLOGO';
import westernLOGO from '@salesforce/resourceUrl/westernLOGO';
import oopsLogo from '@salesforce/resourceUrl/oopsLogo';
import animatedLOGO from '@salesforce/resourceUrl/animatedLOGO';
import mysteryLOGO from '@salesforce/resourceUrl/mysteryLOGO';
import musicLOGO from '@salesforce/resourceUrl/musicLOGO';
export default class MovieTorrent extends LightningElement {
    @track dublicate;
    @track logo = torrentLOGO;
    @track dataVar = [];
    @track dataVar2 = [];
    @track dataVar3 = [];
    @track dataVar4 = [];
    @track grid2Divide = [1, 2];
    @track topSeedFlag = false;
    @track threeDFlag = false;
    @track downloadMovieName;
    @track downloadMovieYear;
    @track downloadMovieRating;
    @track downloadMovieRuntime;
    @track downloadMovieSummary;
    @track downloadMovieLanguage;
    @track downloadMovieUrl;
    @track downloadMovieQuality;
    @track downloadMovieSeeds;
    @track downloadMoviePeers;
    @track downloadMovieSize;
    @track downloadMovieDateUploaded;
    @track downloadMovieImage;
    @track isModalOpen = false;
    @track searchMovieVar = null;
    @track movieSearchFlag = false;
    @track actionPoster = actionLOGO;
    @track comedyPoster = comedyLOGO;
    @track thrillerPoster = thrillerLOGO;
    @track crimePoster = crimeLOGO;
    @track biographyPoster = biographyLOGO;
    @track dramaPoster = dramaLOGO;
    @track horrorPoster = horrorLOGO;
    @track scifiPoster = scifiLOGO;
    @track adventurePoster = adventureLOGO;
    @track romancePoster = romanceLOGO;
    @track fantasyPoster = fantasyLOGO;
    @track westernPoster = westernLOGO;
    @track animatedPoster = animatedLOGO;
    @track mysteryPoster = mysteryLOGO;
    @track musicPoster = musicLOGO;
    @track homePageFlag = true;
    @track genreFlag = false;
    @track imbdSymbol = imbdLOGO;
    @track oopsSymbol = oopsLogo;
    @track genreMovieVar;
    @track oopsModal = false;
    @track qualityVar;

    movieClick() {
        this.topSeedFlag = true;
        this.threeDFlag = false;
        this.movieSearchFlag = false;
        this.homePageFlag = false;
        this.genreFlag = false;
        this.oopsModal = false;
        fun({

        }).then(result => {
            this.dublicate = result;
            console.log('result-->' + result.data.movies[0].title_long);
            for (let x = 0; x < 20; x++) {
                this.dataVar.push({
                    //   movieName: result.data.movies[x].title_long, value: result.data.movies[x].title_long,
                    //   movieYear: result.data.movies[x].year, value: result.data.movies[x].year,
                    //   movieRating: result.data.movies[x].rating, value: result.data.movies[x].rating,
                    //   movieRuntime: result.data.movies[x].runtime, value: result.data.movies[x].runtime,
                    //      movieSummary: result.data.movies[x].summary, value: result.data.movies[x].summary,
                    //   movieLanguage: result.data.movies[x].language, value: result.data.movies[x].language,
                    movieImage: result.data.movies[x].large_cover_image, value: result.data.movies[x].large_cover_image


                    // movieUrl: result.data.movies[x].torrents[0].url, value: result.data.movies[x].torrents[0].url,
                    //   movieQuality: result.data.movies[x].torrents[0].quality, value: result.data.movies[x].torrents[0].quality,
                    //   movieSeeds: result.data.movies[x].torrents[0].seeds, value: result.data.movies[x].torrents[0].seeds,
                    //    moviePeers: result.data.movies[x].torrents[0].peers, value: result.data.movies[x].torrents[0].peers,
                    //    movieSize: result.data.movies[x].torrents[0].size, value: result.data.movies[x].torrents[0].size,
                    //    movieDateUploaded: result.data.movies[x].torrents[0].date_uploaded, value: result.data.movies[x].torrents[0].date_uploaded
                });
            }
        });
    }

    movieDownloadClick(event) {
        this.isModalOpen = true;
        console.log('inside movieDownloadClick--->' + this.dublicate);
        console.log('inside event.target.dataset.item--->' + event.currentTarget.dataset.item);
        for (let x = 0; x < 20; x++) {
            if (this.dublicate.data.movies[x].large_cover_image === event.target.dataset.item) {
                console.log('inside if-->' + event.target.dataset.item);
                this.downloadMovieName = this.dublicate.data.movies[x].title_long;
                this.downloadMovieYear = this.dublicate.data.movies[x].year;
                this.downloadMovieRating = this.dublicate.data.movies[x].rating + " ";
                this.downloadMovieRuntime = this.dublicate.data.movies[x].runtime;
                this.downloadMovieSummary = this.dublicate.data.movies[x].summary;
                this.downloadMovieLanguage = this.dublicate.data.movies[x].language;
                this.downloadMovieUrl = this.dublicate.data.movies[x].torrents[0].url;
                this.downloadMovieQuality = this.dublicate.data.movies[x].torrents[0].quality;
                this.downloadMovieSeeds = this.dublicate.data.movies[x].torrents[0].seeds;
                this.downloadMoviePeers = this.dublicate.data.movies[x].torrents[0].peers;
                this.downloadMovieSize = this.dublicate.data.movies[x].torrents[0].size;
                this.downloadMovieDateUploaded = this.dublicate.data.movies[x].torrents[0].date_uploaded;
                this.downloadMovieImage = this.dublicate.data.movies[x].large_cover_image;
            }
        }
        console.log('dublicate-->' + this.dublicate);
    }





    threeDmovieClick() {
        this.topSeedFlag = false;
        this.threeDFlag = true;
        this.movieSearchFlag = false;
        this.homePageFlag = false;
        this.genreFlag = false;
        this.oopsModal = false;
        this.dataVar2 = [];
        fun1({
            qualityVar: this.qualityVar
        }).then(result => {
            this.dublicate = result;
            console.log('result-->' + result.data.movies[0].title_long);
            for (let x = 0; x < 20; x++) {
                this.dataVar2.push({
                    //    movieName: result.data.movies[x].title_long, value: result.data.movies[x].title_long,
                    //    movieYear: result.data.movies[x].year, value: result.data.movies[x].year,
                    //  movieRating: result.data.movies[x].rating, value: result.data.movies[x].rating,
                    //movieRuntime: result.data.movies[x].runtime, value: result.data.movies[x].runtime,
                    // movieSummary: result.data.movies[x].summary, value: result.data.movies[x].summary,
                    //  movieLanguage: result.data.movies[x].language, value: result.data.movies[x].language,
                    movieImage: result.data.movies[x].large_cover_image, value: result.data.movies[x].large_cover_image,


                    // movieUrl: result.data.movies[x].torrents[0].url, value: result.data.movies[x].torrents[0].url,
                    //  movieQuality: result.data.movies[x].torrents[0].quality, value: result.data.movies[x].torrents[0].quality,
                    // movieSeeds: result.data.movies[x].torrents[0].seeds, value: result.data.movies[x].torrents[0].seeds,
                    //  moviePeers: result.data.movies[x].torrents[0].peers, value: result.data.movies[x].torrents[0].peers,
                    //  movieSize: result.data.movies[x].torrents[0].size, value: result.data.movies[x].torrents[0].size,
                    //  movieDateUploaded: result.data.movies[x].torrents[0].date_uploaded, value: result.data.movies[x].torrents[0].date_uploaded
                });
            }
        });
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
    }
    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isModalOpen = false;
    }

    searchFunMovie() {
        this.movieSearchFlag = true;
        this.topSeedFlag = false;
        this.threeDFlag = false;
        this.homePageFlag = false;
        this.genreFlag = false;
        this.oopsModal = false;
        this.dataVar3 = [];
        console.log('this.datavar3-->' + this.dataVar3);
        console.log('inside searchFunMovie');
        searchMovieFun({
            searchMovieVar: this.searchMovieVar
        }).then(result => {
            this.dublicate = result;
            console.log('resultStatus-->' + result.status);
            //   console.log('result-->' + result.data.movies);
            if (result.status === "not find") {
                this.oopsModal = true;
                console.log('not find');
            }
            // console.log('status of result-->'+ result.data.movies[0].title_long);
            else {
                result.data.movies.forEach(infoData => {
                    this.dataVar3.push({
                        movieName: infoData.title_long, value: infoData.title_long,
                        movieYear: infoData.year, value: infoData.year,
                        movieRating: infoData.rating, value: infoData.rating,
                        movieRuntime: infoData.runtime, value: infoData.runtime,
                        movieSummary: infoData.summary, value: infoData.summary,
                        movieLanguage: infoData.language, value: infoData.language,
                        movieImage: infoData.large_cover_image, value: infoData.large_cover_image,


                        movieUrl: infoData.torrents[0].url, value: infoData.torrents[0].url,
                        movieQuality: infoData.torrents[0].quality, value: infoData.torrents[0].quality,
                        movieSeeds: infoData.torrents[0].seeds, value: infoData.torrents[0].seeds,
                        moviePeers: infoData.torrents[0].peers, value: infoData.torrents[0].peers,
                        movieSize: infoData.torrents[0].size, value: infoData.torrents[0].size,
                        movieDateUploaded: infoData.torrents[0].date_uploaded, value: infoData.torrents[0].date_uploaded
                    });
                });
            }

        });
    }


    searchFunMovie1(event) {
        if (event.keyCode === 13) {

            console.log("inside key press");
            this.movieSearchFlag = true;
            this.topSeedFlag = false;
            this.threeDFlag = false;
            this.homePageFlag = false;
            this.genreFlag = false;
            this.oopsModal = false;
            this.dataVar3 = [];
            this.searchMovieVar = event.target.value;
            console.log('searchevent-->' + this.searchMovieVar);
            console.log('this.datavar3-->' + this.dataVar3);
            console.log('inside searchFunMovie');
            searchMovieFun({
                searchMovieVar: this.searchMovieVar
            }).then(result => {
                this.dublicate = result;
                console.log('resultStatus-->' + result.status);
                //   console.log('result-->' + result.data.movies);
                if (result.status === "not find") {
                    this.oopsModal = true;
                    console.log('not find');
                }
                // console.log('status of result-->'+ result.data.movies[0].title_long);
                else {
                    result.data.movies.forEach(infoData => {
                        this.dataVar3.push({
                            movieName: infoData.title_long, value: infoData.title_long,
                            movieYear: infoData.year, value: infoData.year,
                            movieRating: infoData.rating, value: infoData.rating,
                            movieRuntime: infoData.runtime, value: infoData.runtime,
                            movieSummary: infoData.summary, value: infoData.summary,
                            movieLanguage: infoData.language, value: infoData.language,
                            movieImage: infoData.large_cover_image, value: infoData.large_cover_image,


                            movieUrl: infoData.torrents[0].url, value: infoData.torrents[0].url,
                            movieQuality: infoData.torrents[0].quality, value: infoData.torrents[0].quality,
                            movieSeeds: infoData.torrents[0].seeds, value: infoData.torrents[0].seeds,
                            moviePeers: infoData.torrents[0].peers, value: infoData.torrents[0].peers,
                            movieSize: infoData.torrents[0].size, value: infoData.torrents[0].size,
                            movieDateUploaded: infoData.torrents[0].date_uploaded, value: infoData.torrents[0].date_uploaded
                        });
                    });
                }

            });
        }
    }




    textSearchFun(event) {
        console.log('event-->' + event.target.value);
        this.searchMovieVar = event.target.value;
        console.log('searchMovieVar-->' + this.searchMovieVar);
    }
    homeFunction() {
        this.homePageFlag = true;
        this.movieSearchFlag = false;
        this.topSeedFlag = false;
        this.threeDFlag = false;
        this.genreFlag = false;
        this.oopsModal = false;
    }
    selectGenreFun(event) {
        this.dataVar4 = [];
        this.homePageFlag = false;
        this.movieSearchFlag = false;
        this.topSeedFlag = false;
        this.threeDFlag = false;
        this.genreFlag = true;
        this.oopsModal = false;
        if (event.target.dataset.item === "action") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "comedy") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "thriller") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "crime") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "biography") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "drama") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "horror") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "Sci-Fi") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "adventure") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "fantasy") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "western") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "animation") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "mystery") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else if (event.target.dataset.item === "music") {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        else {
            console.log('in action event.target.dataset.item' + event.target.dataset.item);
            this.genreMovieVar = event.target.dataset.item;
        }
        clickGenreMovie({
            genreMovieVar: this.genreMovieVar
        }).then(result => {
            this.dublicate = result;
            console.log('result-->' + result.data.movies[0].title_long);
            for (let x = 0; x < 20; x++) {
                this.dataVar4.push({
                    movieImage: result.data.movies[x].large_cover_image, value: result.data.movies[x].large_cover_image
                });
            }
        });
    }
    qualityChange(event) {
        console.log('qualityName-->' + event.target.value);
        this.qualityVar = event.target.value;
        this.threeDmovieClick();
    }
}