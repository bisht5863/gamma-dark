import { LightningElement } from 'lwc';
import fun from '@salesforce/apex/Task1Apex.fun';
export default class createRecordForm extends LightningElement {
    steps = [
        { label: 'Contacted', value: 'step-1' },
        { label: 'Open', value: 'step-2' },
        { label: 'Unqualified', value: 'step-3' },
        { label: 'Nurturing', value: 'step-4' },
        { label: 'Closed', value: 'step-5' },
    ];
    opportunityId;
    productId;
    step1=1;
    
    flag1=true;
    flag2=false;
    flag3=false;
    isModalOpen = false;
    handleSuccess(event) {
        this.opportunityId = event.detail.id;
        this.step1=1;
        this.flag1=false;
        this.flag2=true;
    }
    handleSuccess1(event) {
        this.productId = event.detail.id;
        this.step1=2;
        this.flag2=false;
        fun({

            proId : this.productId,
            oppId : this.opportunityId
        })
        this.flag3=true;
        this.step1=3;
    }
    openModal() {
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
    }
    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isModalOpen = false;
    }
}