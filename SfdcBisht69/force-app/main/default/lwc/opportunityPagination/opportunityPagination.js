import { LightningElement, wire } from 'lwc';
import getOpportunityList from '@salesforce/apex/GetCustomOpportunityList.getOpportunityList';
const columns = [
    { label: 'Opportunity Name', fieldName: 'Name', editable: true },
    { label: 'Account Name', fieldName: 'AccountName', editable: true },
    { label: 'Account Site', fieldName: 'Website', type: 'text', },
    { label: 'Stage', fieldName: 'StageName', editable: true },
    { label: 'Close Date', fieldName: 'CloseDate', type: 'Date', editable: true },
    { label: 'Opportuity Owner Alias', fieldName: 'OpportunityOwner' },
];
export default class OpportunityPagination extends LightningElement {
    data = [];
    columns = columns;
    rowOffset = 0;
    connectedCallback() {
        let currentData = [];
        getOpportunityList({
        })
            .then(result => {
                result.forEach((row) => {

                    /* 
                    * Creating the an empty object
                    * To reslove "TypeError: 'set' on proxy: trap returned falsish for property"
                    */

                    let rowData = {};

                    rowData.Name = row.Name;
                    rowData.Id = row.Id;
                    rowData.CloseDate = row.CloseDate;
                    rowData.StageName = row.StageName;
                    // Account related data
                  //  if (row.Product2) {
                        rowData.AccountName = row.Account.Name;
                        rowData.Website = row.Account.ProductCode;
                        rowData.OpportunityOwner = row.OwnerId;
                    //}


                    currentData.push(rowData);
                });
                this.data = currentData;
                console.log('Result----' + result);
            })
    }
}