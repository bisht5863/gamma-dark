import { LightningElement,track } from 'lwc';
import torrentLOGO from '@salesforce/resourceUrl/torrentLOGO';
export default class LoginLogoImage extends LightningElement {
 @track logo = torrentLOGO;
}