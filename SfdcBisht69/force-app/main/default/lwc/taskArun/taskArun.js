import { LightningElement, track } from 'lwc';
export default class TaskArun extends LightningElement {

    result = 0;
    val = [];
    resultValue = [];
    totalField = 0;

    handleClick(event) {
        if (event.target.label === "addFun") {
            this.result = 0;
        }
        else if (event.target.label === "subFun") {
            this.result = this.resultValue[0] * 2;
        }
        else if (event.target.label === "multiFun") {
            this.result = 1;
        }
        else if (event.target.label === "dividFun") {
            this.result = Math.pow(this.resultValue[0], 2);
        }
        for (let x = 0; x < this.totalField; x++) {
            if (event.target.label === "addFun") {
                this.result = this.result + Number(this.resultValue[x]);
            }
            else if (event.target.label === "subFun") {
                this.result = this.result - this.resultValue[x];
            }
            else if (event.target.label === "multiFun") {
                this.result = this.result * this.resultValue[x];
            }
            else if (event.target.label === "dividFun") {
                this.result = this.result / this.resultValue[x];
            }

        }

    }

    onchangeFun(event) {
        for (let x = 0; x < this.totalField; x++) {
            if (x === event.target.name) {
                this.resultValue[x] = event.target.value;
            }
        }
    }

    getUserval(event) {
        this.val = [];
        for (var x = 0; x < event.target.value; x++) {
            this.val.push(x);
        }
        this.totalField = event.target.value;
    }
}