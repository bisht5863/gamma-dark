import { LightningElement, track } from 'lwc';
import fun from '@salesforce/apex/PexelsClass.fun';
export default class PexelLwc extends LightningElement {
    @track imageName;
    @track listImageUrl = [];
    @track listImageUrl2 = [];
    enterNameFun(event) {
        this.imageName = event.target.value;
    }
    searchClick() {
        let x = 0;
        fun({
            imageName: this.imageName
        }).then(data => {
            this.imageUrl = data;
            console.log('data-->' + data);
            data.forEach(listImageName => {
                x++;
                if (x < 5) {
                    console.log('listImageName--->' + listImageName);
                    this.listImageUrl.push({
                        label: listImageName, value: listImageName
                    });
                }
                else {
                    console.log('listImageName2--->' + listImageName);
                    this.listImageUrl2.push({
                        label: listImageName, value: listImageName
                    });
                }
            });

        })
    }
}