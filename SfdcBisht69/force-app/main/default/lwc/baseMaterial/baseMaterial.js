import { LightningElement } from 'lwc';
export default class BaseMaterial extends LightningElement {
    coloeStore;
    col = 'white';
    showColorFlag = false;
    crimsonFunction() {
        this.coloeStore = 'Solid Fill PVC';
        this.col = '#DC143C';
        this.showColorFlag = false;
    }
    yellowFunction() {
        console.log('inside fun2');
        this.coloeStore = 'Solid Fill PVC';
        this.col = '#FFFF00';
        this.showColorFlag = false;
    }
    palevioletredFunction() {
        console.log('inside fun3');
        this.coloeStore = 'Solid Fill PVC';
        this.col = '#DB7093';
        this.showColorFlag = false;
    }
    mediumblueFunction() {
        console.log('inside blackCovertIRCovertFunction');
        this.coloeStore = 'Solid Fill PVC';
        this.col = '#0000CD';
        this.showColorFlag = false;
    }
    lightseagreenFunction() {
        console.log('inside blackCovertIRCovertFunction');
        this.coloeStore = 'Solid Fill PVC';
        this.col = '#20B2AA';
        this.showColorFlag = false;
    }
    blackFunction() {
        console.log('inside blackCovertIRCovertFunction');
        this.coloeStore = 'Solid Fill PVC';
        this.col = '#000000';
        this.showColorFlag = false;
    }


    whiteFunction() {
        this.coloeStore = 'White HiVis SOLAS';
        this.col = '#FFFFFF';
        this.showColorFlag = false;
    }
    blueFunction() {
        console.log('inside fun2');
        this.coloeStore = 'Blue HiVis SOLAS';
        this.col = '#0000FF';
        this.showColorFlag = false;
    }
    greenFunction() {
        console.log('inside fun3');
        this.coloeStore = 'Green HiVis SOLAS';
        this.col = '#008000';
        this.showColorFlag = false;
    }
    redFunction() {
        console.log('inside blackCovertIRCovertFunction');
        this.coloeStore = 'Red HiVis SOLAS';
        this.col = '#FF0000';
        this.showColorFlag = false;
    }
    lightseagreenFunction() {
        console.log('inside blackCovertIRCovertFunction');
        this.coloeStore = 'Solid Fill PVC';
        this.col = '#20B2AA';
        this.showColorFlag = false;
    }
    blackFunction() {
        console.log('inside blackCovertIRCovertFunction');
        this.coloeStore = 'Solid Fill PVC';
        this.col = '#000000';
        this.showColorFlag = false;
    }
    exteriorClick() {
        this.showColorFlag = true;
    }
}