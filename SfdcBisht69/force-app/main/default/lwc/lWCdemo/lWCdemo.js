import { LightningElement } from 'lwc';
import fun from '@salesforce/apex/MailApex.fun';
export default class LWCdemo extends LightningElement {
    flag;
    height;
    width;
    handleChange(event)
    {
        this.flag=event.target.value;
        console.log('color number--->'+this.flag);
    }
    changeFun(event)
    {
        if(event.target.name==='Height')
        {
            this.height=event.target.value;
            console.log('this.height-->'+this.height);
        }
        else{
            this.width=event.target.value;
            console.log('this.width-->'+this.width);
        }
    }

     handleClick() {
         console.log('i am in handleclick');
        fun({

            colorCode : this.flag,
            height : this.height,
            width : this.width

        })
     }
     handleUploadFinished()
     {
         //alert('file uploaded');
     }
}