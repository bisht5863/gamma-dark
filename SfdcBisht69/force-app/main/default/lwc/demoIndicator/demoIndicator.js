import { LightningElement, track } from 'lwc';
import fun from '@salesforce/apex/Task1Apex.fun';
export default class LWCProgressIndicator extends LightningElement {
    contact = {};
    selectedStep = 'Step1';
    oppStageName;
    flag1 = true;
    flag2 = false;
    flag3 = false;
    flag4 = false;
    keyflag = true;
    oppName;
    oppDate;
    oppDescription;
    proName;
    oppStageName;
    proQuantity;
    proPrice;
    customFlag = false;
    get options() {
        return [
            { label: 'Prospecting', value: 'Prospecting' },
            { label: 'Qualification', value: 'Qualification' },
            { label: 'Needs Analysis', value: 'Needs Analysis' },
            { label: 'Value Proposition', value: 'Value Proposition' },
            { label: 'Id. Decision Makers', value: 'Id. Decision Makers' },
            { label: 'Perception Analysis', value: 'Perception Analysis' },
            { label: 'Proposal/Price Quote', value: 'Proposal/Price Quote' },
            { label: 'Negotiation/Review', value: 'Negotiation/Review' },
            { label: 'Closed Won', value: 'Closed Won' },
            { label: 'Closed Won', value: 'Closed Won' }
        ];
    }

    handleChange(event) {
        console.log("handleChange");
        if (event.target.name === 'oppName') {
            this.oppName = event.target.value;
        }
        else if (event.target.name === 'oppDate') {
            this.oppDate = event.target.value;
            console.log('type--->' + typeof this.oppDate);
            console.log(this.oppDate);
        }
        else if (event.target.name === 'progress') {
            this.oppStageName = event.detail.value;
        }

        else if (event.target.name === 'proPrice') {
            this.proPrice = event.target.value;
        }
        else if (event.target.name === 'proQuantity') {
            this.proQuantity = event.target.value;
        }
        else {
            this.proName = event.target.value;
        }

    }
    handleNext() {
        if (this.isInputValid()) {
            console.log(this.contact);
            this.customFlag = true;
        }

        if (this.isInputValid() === true) {
            var getselectedStep = this.selectedStep;
            if (getselectedStep === 'Step1') {
                this.selectedStep = 'Step2';
                this.flag1 = false;
                this.flag2 = true;
            }
            else if (getselectedStep === 'Step2') {
                this.selectedStep = 'Step3';
                this.flag2 = false;
                this.flag3 = true;
            }
            else if (getselectedStep === 'Step3') {
                this.selectedStep = 'Step4';
                this.flag3 = false;
                this.flag4 = true;
            }
        }
    }

    handlePrev() {
        var getselectedStep = this.selectedStep;
        if (getselectedStep === 'Step2') {
            this.selectedStep = 'Step1';
            this.flag1 = true;
            this.flag2 = false;
        }
        else if (getselectedStep === 'Step3') {
            this.selectedStep = 'Step2';
            this.flag2 = true;
            this.flag3 = false;
        }
        else if (getselectedStep === 'Step4') {
            this.selectedStep = 'Step3';
            this.flag3 = true;
            this.flag4 = false;
        }
    }

    handleFinish() {
        this.keyflag = false;
        alert('Finished...');
        fun({

            oppName: this.oppName,
            oppDate: this.oppDate,
            oppStageName: this.oppStageName,
            oppDescription: this.oppDescription,
            proName: this.proName,
            proQuantity: this.proQuantity,
            proPrice: this.proPrice,

        })
        this.selectedStep = 'Step1';
        this.isModalOpen = false;
        eval("$A.get('e.force:refreshView').fire();");
    }

    selectStep1() {
        this.selectedStep = 'Step1';
    }

    selectStep2() {
        this.selectedStep = 'Step2';
    }

    selectStep3() {
        this.selectedStep = 'Step3';
    }

    selectStep4() {
        this.selectedStep = 'Step4';
    }

    get isSelectStep4() {
        return this.selectedStep === "Step4";
    }
    stepFun(event) {
        if (this.isInputValid() === true) {
            if (event.target.label === 'Creating Opportunity') {
                this.flag1 = true;
                this.flag2 = false;
                this.flag3 = false;
                this.flag4 = false;
                console.log('inside 1');
            }
            else if (event.target.label === 'Creating Product') {
                this.flag1 = false;
                this.flag2 = true;
                this.flag3 = false;
                this.flag4 = false;
                console.log('inside 2');
            }
            else if (event.target.label === 'Creating OpportunityLineItem') {
                this.flag1 = false;
                this.flag2 = false;
                this.flag3 = true;
                this.flag4 = false;
                console.log('inside 3');
            }
            else {
                this.flag1 = false;
                this.flag2 = false;
                this.flag3 = false;
                this.flag4 = true;
            }

            console.log('inside stepFun');
        }
    }
    isInputValid() {
        let isValid = true;
        let inputFields = this.template.querySelectorAll('.validate');
        inputFields.forEach(inputField => {
            if (!inputField.checkValidity()) {
                inputField.reportValidity();
                isValid = false;
            }
            this.contact[inputField.name] = inputField.value;
        });
        return isValid;
    }
    isModalOpen = false;
    openModal() {
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
    }
}