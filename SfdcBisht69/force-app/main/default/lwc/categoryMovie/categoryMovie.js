import { LightningElement, track } from 'lwc';
import actionLOGO from '@salesforce/resourceUrl/actionLOGO';
import comedyLOGO from '@salesforce/resourceUrl/comedyLOGO';
import thrillerLOGO from '@salesforce/resourceUrl/thrillerLOGO';
import crimeLOGO from '@salesforce/resourceUrl/crimeLOGO';
import biographyLOGO from '@salesforce/resourceUrl/biographyLOGO';
import dramaLOGO from '@salesforce/resourceUrl/dramaLOGO';
import horrorLOGO from '@salesforce/resourceUrl/horrorLOGO';
import scifiLOGO from '@salesforce/resourceUrl/scifiLOGO';
import adventureLOGO from '@salesforce/resourceUrl/adventureLOGO';
import romanceLOGO from '@salesforce/resourceUrl/romanceLOGO';
import torrentLOGO from '@salesforce/resourceUrl/torrentLOGO';
export default class CategoryMovie extends LightningElement {
    @track actionPoster = actionLOGO;
    @track comedyPoster=comedyLOGO;
    @track thrillerPoster=thrillerLOGO;
    @track crimePoster=crimeLOGO;
    @track biographyPoster=biographyLOGO;
    @track dramaPoster=dramaLOGO;
    @track horrorPoster=horrorLOGO;
    @track scifiPoster=scifiLOGO;
    @track adventurePoster=adventureLOGO;
    @track romancePoster=romanceLOGO;
    @track logo = torrentLOGO;
}