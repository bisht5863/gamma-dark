import { LightningElement, track } from 'lwc';
import votingFunction from '@salesforce/apex/VoteSuper.votingFunction';
import resultMethod from '@salesforce/apex/ResultSuperClass.resultMethod';
export default class PollSuperHero extends LightningElement {
    @track heroData = [];
    @track imageDemo = '';
    @track imageDemo1 = '';
    @track firstHero = '';
    @track secondHero = '';
    startPoll() {
        votingFunction({

        }).then(result => {
            this.dublicate = result;
            result.forEach(infoData => {
                this.heroData.push({
                    id: infoData.id,
                    name: infoData.name,
                    image: infoData.images.lg
                })
            });
        });
    }
    startPoll1() {
        console.log('heroData-->' + this.heroData);
        console.log('Math.floor(Math.random() * 700)-->' + Math.floor(Math.random() * 700));
        var firstVar = Math.floor(Math.random() * 700);
        var secondVar = Math.floor(Math.random() * 700);
        this.imageDemo = this.heroData[firstVar].image;
        this.imageDemo1 = this.heroData[secondVar].image;
        this.firstHero = this.heroData[firstVar].name;
        this.secondHero = this.heroData[secondVar].name;
    }
    firstConsist() {
        resultMethod({
            winner: this.firstHero,
            loser: this.secondHero
        })
    }
    secondConsist() {
        resultMethod({
            winner: this.secondHero,
            loser: this.firstHero
        })
    }
    testingMethod(){
        console.tabel('this is code-->'+this.template.querySelectorAll('div'));
    }
}