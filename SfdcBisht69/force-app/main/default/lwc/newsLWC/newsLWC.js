import { LightningElement, track } from 'lwc';
import fun from '@salesforce/apex/NewsClass.fun';

export default class NewsLWC extends LightningElement {
    @track arraydata = [];
    @track isModalOpen = false;
    newsClick() {
        this.isModalOpen = true;
        fun({

        }).then(data => {
            console.log('lang-->' + data[0].title);
            data.forEach(newLoopVar => {
                console.log('newLoopVar-->' + newLoopVar.title);
                this.arraydata.push({
                    title: newLoopVar.title,
                    source: newLoopVar.source
                });
            });
        })
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
    }
    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isModalOpen = false;
    }
}