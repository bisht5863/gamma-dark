import { LightningElement, track } from 'lwc';
import mailFun from '@salesforce/apex/YtsMail.mailFun';
export default class MovieTorrentFooter extends LightningElement {
    @track isFeedBackModal = false;
    @track myVal;
    @track feedName = '';
    @track feedEmail = '';
    @track feedSubject = '';
    closeModal() {
        this.isFeedBackModal = false;
    }
    submitDetails() {
        let mailList = [this.feedName, this.feedSubject, this.feedEmail, this.myVal];
        this.isFeedBackModal = false;
        console.log('inside submit detail');
        mailFun({
            mailList: mailList
        })
        console.log('after inside submit detail' + mailList);
    }
    feedbackFUN() {
        this.isFeedBackModal = true;
        console.log('inside the feedbackFUN');
    }
    richTextFUN(event) {
        this.myVal = event.target.value;
        console.log('myval-->' + this.myVal);
    }
    textFun(event) {
        if (event.target.name === 'name') {
            this.feedName = event.target.value;
            console.log('feedName-->' + this.feedName);
        }
        else if (event.target.name === 'email') {
            this.feedEmail = event.target.value;
            console.log('feedEmail-->' + this.feedEmail);
        }
        else {
            this.feedSubject = event.target.value;
            console.log('feedSubject-->' + this.feedSubject);
        }
    }
    createComponent() {
        console.log('this is createComponent ');
    }
}