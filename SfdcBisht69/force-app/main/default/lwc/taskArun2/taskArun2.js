import { LightningElement } from 'lwc';
import methodName from '@salesforce/apex/GetAcc.methodName1';
import methodName2 from '@salesforce/apex/GetAcc.methodName2';
const columnsList = [
    { label: "Name", fieldName: 'Name', editable: true },
    { label: "Website", fieldName: 'Website', editable: true },
    { label: "Phone", fieldName: 'Phone', editable: true },
];
export default class TaskArun2 extends LightningElement {

    valInstance = '';
    dataList = [];
    columnsList = columnsList;
    rowOffset = 0;
    draftvalue = [];
    fun(event) {
        if (event.target.value.length > 2) {
            console.log(event.target.value);
            methodName({
                nameInstance: event.target.value
            })
                .then(data => {
                    console.log('data-->' + JSON.stringify(data));
                    this.dataList = data;
                })
        }
        else{
            this.dataList = [];
        }


    }

    handleSaveEvent(event){
        console.log(event.detail.draftValues);
        console.log(JSON.stringify(event.detail.draftValues));

        methodName2({
            accList : event.detail.draftValues
        })

    }


}