import { LightningElement } from 'lwc';
import MassUploadTenderTemplate from '@salesforce/resourceUrl/lastremnat';
// Example :- import TRAILHEAD_LOGO from '@salesforce/resourceUrl/trailhead_logo';
export default class PuzzelLWC extends LightningElement {
    color1 = 'red';
    color2 = 'green';
    color3 = 'yellow';
    color4 = 'pink';
    colorArray = ['red', 'green', 'yellow', 'pink', 'Orange', 'blue'];
    color1Function() {
        var temp = this.colorArray[Math.floor(Math.random() * this.colorArray.length)];
        this.color1 = temp;
    }
    color2Function() {
        var temp = this.colorArray[Math.floor(Math.random() * this.colorArray.length)];
        this.color2 = temp;
    }
    color3Function() {
        var temp = this.colorArray[Math.floor(Math.random() * this.colorArray.length)];
        this.color3 = temp;
    }
    color4Function() {
        var temp = this.colorArray[Math.floor(Math.random() * this.colorArray.length)];
        this.color4 = temp;
    }
    PressForUnlock() {
        if (this.color1 === 'blue' && this.color2 === 'blue' && this.color3 === 'blue' && this.color4 === 'blue') {
            alert('Successfully unlock');
        }
        else {
            alert('Incorrect Pattern');
        }
    }
    downloadTorrent() {
        const resourcePath = MassUploadTenderTemplate;
        window.open(resourcePath, '_blank');
        return null;
    }
    downloadSaple(){
        const resourcePath = 'https://cacom24-dev-ed--c.documentforce.com/servlet/servlet.FileDownload?file=00P5j000005ZiLXEA0&operationContext=S1';
        window.open(resourcePath, '_blank');
        return null;
    }

}