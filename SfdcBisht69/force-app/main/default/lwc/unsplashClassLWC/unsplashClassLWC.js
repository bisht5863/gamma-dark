import { LightningElement, track } from 'lwc';
import fun from '@salesforce/apex/UnsplashClass.fun';
import fun2 from '@salesforce/apex/UnsplashClass.fun2';
import fun3 from '@salesforce/apex/UnsplashClass.fun3';
import fun4 from '@salesforce/apex/UnsplashClass.fun4';
export default class UnsplashClassLWC extends LightningElement {
    @track isModalOpen = false;
    @track imageUrl;
    @track imageUrl2;
    @track imageUrl3;
    @track imageUrl4;
    openModal() {
        this.isModalOpen = true;
        fun({

        }).then(data => {
            this.imageUrl = data;
            console.log('data-->' + data);
        })
        fun2({

        }).then(data => {
            this.imageUrl2 = data;
            console.log('data2-->' + data);
        })

        fun3({

        }).then(data => {
            this.imageUrl3 = data;
            console.log('data3-->' + data);
        })
        fun4({

        }).then(data => {
            this.imageUrl4 = data;
            console.log('data3-->' + data);
        })
    }
    closeModal() {
        this.isModalOpen = false;
    }
    submitDetails() {
        fun({

        }).then(data => {
            this.imageUrl = data;
            console.log('data-->' + data);
        })

        fun2({

        }).then(data => {
            this.imageUrl2 = data;
            console.log('data2-->' + data);
        })

        fun3({

        }).then(data => {
            this.imageUrl3 = data;
            console.log('data3-->' + data);
        })
        fun4({

        }).then(data => {
            this.imageUrl4 = data;
            console.log('data3-->' + data);
        })
    }
}