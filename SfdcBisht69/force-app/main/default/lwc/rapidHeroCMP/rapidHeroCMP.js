import { LightningElement, track } from 'lwc';
import fun from '@salesforce/apex/RapidSuperHeros.fun';
import fun2 from '@salesforce/apex/RapidSuperHeros.fun2';
import fun3 from '@salesforce/apex/SearchRapidHero.fun3';
export default class RapidHeroCMP extends LightningElement {

    @track arraydata = [];
    @track villiandata = [];
    @track heroName;
    @track isModalOpen = false;
    @track finalHeroName;
    @track finalHeroImage;
    @track finalintelligence;
    @track finalstrength;
    @track finalspeed;
    @track finaldurability;
    @track finalpower;
    @track finalcombat;
    @track finalplaceOfBirth;
    @track finalfirstAppearance;
    @track finalpublisher;
    @track finaloccupation;
    superClick() {
        fun({

        }).then(data => {
            console.log('SuperdataJson--->' + JSON.stringify(data));
            console.log('SuperdataJson nameList--->' + JSON.stringify(data.nameList));
           // console.log('data.nameList[0] nameList--->' + data.nameList[0]);
            for (var x = 0; x < 20; x++) {
                this.arraydata.push({
                    nameList: data.nameList[x],
                    imageList: data.imageList[x],
                    publisherList: data.publisherList[x],
                    placeOfBirthList: data.placeOfBirthList[x],
                    occupationList: data.occupationList[x]
                });
            }
        })
    }
    villianClick() {
        fun2({

        }).then(data => {
            console.log('villiandataJson--->' + JSON.stringify(data));
            console.log('villiandataJson nameList--->' + JSON.stringify(data.nameList));
            for (var x = 0; x < 20; x++) {
                this.villiandata.push({
                    nameList: data.nameList[x],
                    imageList: data.imageList[x],
                    publisherList: data.publisherList[x],
                    placeOfBirthList: data.placeOfBirthList[x],
                    occupationList: data.occupationList[x]
                });
            }
        })
    }
    heroOnChange(event) {
        this.heroName = event.target.value;
    }
    heroSearchClick() {
        this.isModalOpen = true;
        fun3({
            heroName: this.heroName
        }).then(data => {
            console.log('data-->' + data);
            this.finalHeroImage = data[0];
            this.finalHeroName = data[1].toUpperCase();
            this.finalintelligence = data[2];
            this.finalstrength = data[3];
            this.finalspeed = data[4];
            this.finaldurability = data[5];
            this.finalpower = data[6];
            this.finalcombat = data[7];
            this.finalplaceOfBirth = data[8];
            this.finalfirstAppearance = data[9];
            this.finalpublisher = data[10];
            this.finaloccupation = data[11];
        })
    }
    closeModal() {
        this.isModalOpen = false;
    }
    submitDetails() {
        this.isModalOpen = false;
    }
}