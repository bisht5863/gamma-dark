public class pageDisplay {
@AuraEnabled
public static list<String> fun()
{
    string productids='01t5j000000nG1MAAU';
    list<ProductMedia> productMediaList=new list<ProductMedia>();
    productMediaList=[select ElectronicMediaId from ProductMedia where productid=:productids limit 1000];
    list<string> ProductUrlList=new list<string>();
    for(ProductMedia productMediaRecord: productMediaList){
      string ProductUrl='https://cacom24-dev-ed.lightning.force.com/cms/delivery/media/'+B2BCMSContentService.getContentServiceImage(productMediaRecord.ElectronicMediaId).items[0].contentKey; 
      ProductUrlList.add(ProductUrl);
    }

    
    return ProductUrlList;
}
}