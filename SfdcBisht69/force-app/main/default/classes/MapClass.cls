public class MapClass {
    public static void fun(){
        List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
        oppLineItemList = [SELECT ID,OpportunityId,Quantity FROM OpportunityLineItem where OpportunityId!= Null];
        Set<String> oppSet = new Set<String>();
        Map<String,Double> oppMap = new Map<String,Double>();
        for(OpportunityLineItem oppLine : oppLineItemList){
            oppSet.add(oppLine.OpportunityId);
            if(!oppMap.containsKey(oppLine.OpportunityId)){
                oppMap.put(oppLine.OpportunityId,oppLine.Quantity);
            }
            else{
                oppMap.put(oppLine.OpportunityId,oppMap.get(oppLine.OpportunityId)+oppLine.Quantity);
            }
        }
        List<Opportunity> oppList = new List<Opportunity>();
        oppList = [SELECT Id,AccountId,Name from Opportunity where AccountId!=Null and Id IN:oppSet];
        set<String> accSet = new set<String>();
        Map<String,double> accMap = new Map<String,Double>();
        for(Opportunity oppTemp : oppList){
            accSet.add(oppTemp.AccountId);
            if(!accMap.containsKey(oppTemp.AccountId)){
                accMap.put(oppTemp.AccountId,oppMap.get(oppTemp.Id));
            }
            else{
                accMap.put(oppTemp.AccountId,oppMap.get(oppTemp.Id)+accMap.get(oppTemp.AccountId));
            }
        }
        system.debug('account Map-->'+accMap);
        List<Account> accList = new List<Account>();
        accList = [SELECT id,Name From Account where Id IN:accSet];
        for(Account acc :accList){
            system.debug('Name->'+acc.Name+', Quantity'+accMap.get(acc.Id));
        }
    }
}