public class ContactTriggerHandler {
    public static void lastNameConcatMethod(List<Contact> conList){
        Map<Id,String> map1 = new Map<Id,String>();
        List<Account> updateAccList = new List<Account>();
        for(Contact con : [SELECT Id,LastName,AccountId FROM Contact WHERE Id IN: conList AND AccountId != NULL]){
            if(!map1.containsKey(con.AccountId)){
                map1.put(con.AccountId,' '+con.LastName);
            }
            else{
                map1.put(con.AccountId,map1.get(con.AccountId)+' '+con.LastName);
            }
        }
        for(Account accInstance : [SELECT Id,Name FROM Account WHERE Id IN: map1.keySet()]){
            accInstance.Name = accInstance.Name + ' '+map1.get(accInstance.Id);
            updateAccList.add(accInstance);
        }
        update updateAccList;
    }
    public static void updateAccountNameMethod(Map<Id,Contact> newMap, Map<Id,Contact> oldMap){
        Map<Id,String> map1 = new Map<Id,String>();
        List<Account> updateAccList = new List<Account>();
        for(Contact con : [SELECT Id,LastName,AccountId,Account.Name FROM Contact WHERE Id IN: newMap.keySet() AND AccountId != NULL]){
            String accName = con.Account.Name.replace(oldMap.get(con.Id).LastName,newMap.get(con.Id).LastName);
            map1.put(con.AccountId,' '+accName);
        }
        for(Account acc : [SELECT Id,Name FROM Account WHERE Id IN: map1.keySet()]){
            acc.Name = map1.get(acc.Id);
            updateAccList.add(acc);
        }
        update updateAccList;
    }
    public static void updateAmountFiled(List<Contact> conList){
        List<AggregateResult> arrList = [SELECT Count(Id) ct,AccountId FROM Contact WHERE AccountId != NULL Group By AccountId];
        Map<Id,Integer> map1 = new Map<Id,Integer>();
        for(AggregateResult arr : arrList){
            map1.put((Id)arr.get('AccountId'),(Integer)arr.get('ct'));
        }
        System.debug('map1-->'+map1.get(conList[0].AccountId));
        List<Account> updateAccList = new List<Account>();
        for(Account acc : [SELECT Id,Amount__c FROM Account WHERE Id IN : map1.keySet()]){
            acc.Amount__c = map1.get(acc.Id);
            updateAccList.add(acc);
        }
        update updateAccList;
    }
    
    public static void updateAmountFiled1(List<Contact> conList){
        List<AggregateResult> arrList = [SELECT Count(Id) ct,AccountId FROM Contact WHERE AccountId != NULL Group By AccountId];
        Map<Id,Integer> map1 = new Map<Id,Integer>();
        for(AggregateResult arr : arrList){
            map1.put((Id)arr.get('AccountId'),(Integer)arr.get('ct'));
        }
        System.debug('map1-->'+map1.get(conList[0].AccountId));
        List<Account> updateAccList = new List<Account>();
        for(Account acc : [SELECT Id,Amount__c FROM Account WHERE Id IN : map1.keySet()]){
            acc.Amount__c = acc.Amount__c - map1.get(acc.Id);
            updateAccList.add(acc);
        }
        update updateAccList;
    }
    
    public static void contactRestrictionMethod(List<Contact> conList){
		Set<Id> accIdSet = new Set<Id>();
		for(Contact con : conList){
			accIdSet.add(con.AccountId);
		}
		Map<Id,Integer> map1 = new Map<Id,Integer>();
		for(Contact con : [SELECT Id,AccountId From Contact WHERE AccountId IN : accIdSet]){
			if(!map1.containsKey(con.AccountId)){
				map1.put(con.AccountId,1);
			}
			else{
				map1.put(con.AccountId,map1.get(con.AccountId) + 1);
			}
		}

		for(Contact con : conList){
			if(map1.get(con.AccountId)>=2){
                System.debug('map1.get(con.AccountId)-->'+map1.get(con.AccountId));
				con.addError('There is already 2 contact in the account');
			}
		}
	}

	public static void contactRestrictionMethod1(List<Contact> conList){
		Set<Id> accIdSet = new Set<Id>();
		for(Contact con : conList){
			accIdSet.add(con.AccountId);
		}
		Map<Id,Integer> map1 = new Map<Id,Integer>();
		for(Contact con : [SELECT Id,AccountId From Contact WHERE AccountId IN : accIdSet]){
			if(!map1.containsKey(con.AccountId)){
				map1.put(con.AccountId,1);
			}
			else{
				map1.put(con.AccountId,map1.get(con.AccountId) + 1);
			}
		}

		for(Contact con : conList){
			if(map1.get(con.AccountId)<=2){
				con.addError('You are not able to delete this contact');
			}
		}
	}
}