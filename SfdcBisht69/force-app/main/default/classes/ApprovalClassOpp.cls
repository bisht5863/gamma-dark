public class ApprovalClassOpp {
    public static void fun(){
        //here
        String recordId = '0065j00000alQ3fAAE';
        List<ProcessInstanceWorkitem> workItems = [
            SELECT Id, ProcessInstanceId 
            FROM ProcessInstanceWorkitem 
            WHERE ProcessInstance.TargetObjectId = :recordId 
        ];
        // Hi Dholu here 
        System.debug('workItems-->'+workItems);
        List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
        for(ProcessInstanceWorkitem workItem : workItems){
            System.debug('workItem==>'+workItem);
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setWorkitemId(workItem.Id);
            //Valid values are: Approve, Reject, or Removed. 
            //Only system administrators can specify Removed.
            req.setAction('Approve');
            req.setComments('Your Comment.');
            requests.add(req);
        }
        Approval.ProcessResult[] processResults = Approval.process(requests);
    }
}