/**
 * @author Suraj Tripathi
 * @date 09/02/2022
 * @description This is helper class of OpportunityOrderTrigger Trigger
 */
public with sharing class OpportunityOrderTriggerHelper {
         /**
       * @description used to create order with orderItem with respect to Opportunity and their OpportunityLineItem
       * @param opportunityList      OpportunityIdList
       */
    public void createOrder(List<Opportunity> opportunityList)
    {
       // system.debug('opportunityList-->'+opportunityList);
        set<id>oppIdSet = new set<id>();
        list<order>demoOrderList=new list<order>();
        list<id>demoOpportunity=new list<id>();
        for(Opportunity demoOpp:opportunityList)
        {
            if(demoOpp.StageName=='Closed Won')
            {
                oppIdSet.add(demoOpp.Id);
                demoOpportunity.add(demoOpp.Id);
                Order demoOrder = new Order();
                demoOrder.AccountId=demoOpp.AccountId;
                demoOrder.OwnerId=demoOpp.OwnerId;
                demoOrder.Pricebook2Id=demoOpp.Pricebook2Id; 
                demoOrder.Status='Draft';
                demoOrder.EffectiveDate=date.today();
                demoOrderList.add(demoOrder);
               // system.debug('demoOrderList.Id-->'+demoOrderList);
               // system.debug('demoOpp.Id-->'+demoOpp.Id);
            }
        }
        insert demoOrderList;
      
//system.debug('demoOrderList-->'+demoOrderList);
      //  system.debug('oppIdSet-->'+oppIdSet);
        
        Map<id,id> orderMap = new Map<id,id>();
        
        Map<id,integer> orderAndItemMap = new Map<id,integer>();
        list<OrderItem>oderItemDemoList=new list<OrderItem>();
        List<OpportunityLineItem> oppLineItemList = new  List<OpportunityLineItem>();
        oppLineItemList = [SELECT ID,Quantity,UnitPrice,PricebookEntryId,OpportunityId,Opportunity.AccountId,Opportunity.OwnerId,Opportunity.Pricebook2Id FROM OpportunityLineItem where OpportunityId IN:OppIdSet WITH SECURITY_ENFORCED];
      //  system.debug('oppLineItemList'+oppLineItemList);
        
        for(OpportunityLineItem demoOppLineItem:oppLineItemList)
        {
            if(!orderAndItemMap.containsKey(demoOppLineItem.OpportunityId))
            {
                orderAndItemMap.put(demoOppLineItem.OpportunityId,1);
            }
            else
            {
                orderAndItemMap.put(demoOppLineItem.OpportunityId,orderAndItemMap.get(demoOppLineItem.OpportunityId)+1); 
            }
        }
        for(integer y=0;y<oppIdSet.size();y++)
        {
            orderMap.put(demoOpportunity.get(y),demoOrderList.get(y).id);
        }
        for(Opportunity opp:opportunityList)
        {
            for(OpportunityLineItem demoOppLineItem:oppLineItemList)
            {
                OrderItem oderItemDemo = new OrderItem();
                oderItemDemo.PricebookEntryId=demoOppLineItem.PricebookEntryId;
                oderItemDemo.OrderId=orderMap.get(demoOppLineItem.OpportunityId);
                oderItemDemo.Quantity=demoOppLineItem.Quantity;
                oderItemDemo.UnitPrice=demoOppLineItem.UnitPrice;
               // system.debug('oderItemDemo.PricebookEntryId='+oderItemDemo.PricebookEntryId+'oderItemDemo.OrderId='+oderItemDemo.OrderId+'oderItemDemo.Quantity='+oderItemDemo.Quantity+'oderItemDemo.UnitPrice='+oderItemDemo.UnitPrice);
                oderItemDemoList.add(oderItemDemo); 
            }
        } 
     //   system.debug('ordermap-->'+orderMap);
       if(Schema.sObjectType.OrderItem.isCreateable())
        {
        insert oderItemDemoList;
        }
       // system.debug('oderItemDemoList-->'+oderItemDemoList);
    }
}