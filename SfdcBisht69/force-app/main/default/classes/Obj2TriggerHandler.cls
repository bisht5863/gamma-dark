public class Obj2TriggerHandler {
    public static void updateOwnerObj1(List<Obj2__c> obj2List){
        Set<String> emailSet = new Set<String>();
        List<Obj2__c>  obj2ListUpdte = [SELECT ID,OwnerId,EMail__c,userId__c from Obj2__c where Id IN : obj2List];
        for(Obj2__c obj2Instance : obj2ListUpdte){
            emailSet.add(obj2Instance.EMail__c);
        }
        List<Obj2__c> obj2UpdateListInstance = new List<Obj2__c>();
        List<Obj1__c> obj1ListInstance = [SELECT Id,EMail__c,User__c FROM Obj1__c WHERE EMail__c IN : emailSet];
        Map<String,id> mapObj1Instance = new  Map<String,id>();
        for(Obj1__c objInstance : obj1ListInstance){
            mapObj1Instance.put(objInstance.EMail__c, objInstance.User__c);
        }
        for(Obj2__c objInstance : obj2ListUpdte){
            if(mapObj1Instance.containsKey(objInstance.EMail__c)){
                objInstance.OwnerId = mapObj1Instance.get(objInstance.EMail__c);
                obj2UpdateListInstance.add(objInstance);
            }
        }
        update obj2UpdateListInstance;
    }
}