public class cardDetailClass {
    @AuraEnabled
    public static void customerDetailsFunction(String customerDetails,List<String> cardDetailsvariable)
    {
        try{
            system.debug('listVar-->'+cardDetailsvariable); 
            Map<String,Object> m = (Map<String,Object>)JSON.deserializeUntyped(customerDetails);
            System.debug('card details-->'+m);
            String customerName=String.valueOf(m.get('customerName'));
            system.debug('customerName-->'+customerName);
            Http h =new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint('https://api.stripe.com/v1/customers');
            req.setHeader('Authorization', 'Bearer ' + 'sk_test_51JuwKmSBDzxwoDrtNY5mR9NtpQ5jHEXY8iRBhZ0KtgraDM9O9yKpbGVNzE9ZNCXXGuFWQbnjjDkI8iIznFVyqzR800y7vqpp9K');
            req.setMethod('POST');
            req.setBody('email='+String.valueOf(m.get('customerEmail'))+'&name='+customerName+'&phone='+String.valueOf(m.get('customerPhone')));
            HttpResponse customerDetailresponseBody = h.send(req);
            String customCustomerId;
            if(customerDetailresponseBody.getStatusCode()==200)
            {
                system.debug('responce body '+customerDetailresponseBody.getBody()); 
                map<string, object> customerDetailJson= (map<string, object>) JSON.deserializeUntyped(customerDetailresponseBody.getBody());
                system.debug('customerDetailJson-->'+customerDetailJson);
                for(String keyStrings:customerDetailJson.keySet())
                {
                    if(keyStrings=='id')
                    {
                        customCustomerId=String.valueOf(customerDetailJson.get(keyStrings));
                        system.debug('id--->'+customerDetailJson.get(keyStrings));
                    }
                }
                
            }
            else{
                system.debug('res error value is-->'+customerDetailresponseBody.getBody());
            }
            Http customHTTP =new Http();
            HttpRequest customRequest = new HttpRequest();
            customRequest.setEndpoint('https://api.stripe.com/v1/tokens');
            customRequest.setHeader('Authorization', 'Bearer ' + 'sk_test_51JuwKmSBDzxwoDrtNY5mR9NtpQ5jHEXY8iRBhZ0KtgraDM9O9yKpbGVNzE9ZNCXXGuFWQbnjjDkI8iIznFVyqzR800y7vqpp9K');
            customRequest.setMethod('POST');
            customRequest.setBody('card[number]='+cardDetailsvariable[0]+'&card[exp_year]='+cardDetailsvariable[1]+'&card[exp_month]='+cardDetailsvariable[2]+'&card[cvc]='+cardDetailsvariable[3]);
            HttpResponse customcardTokenResponse = customHTTP.send(customRequest);
            String customCardTokenId;
            if(customcardTokenResponse.getStatusCode()==200)
            {
                system.debug('card token responce body '+customcardTokenResponse.getBody()); 
                map<string, object> customerTokenMap= (map<string, object>) JSON.deserializeUntyped(customcardTokenResponse.getBody());
                system.debug('customerTokenMap-->'+customerTokenMap);
                for(String keyStrings:customerTokenMap.keySet())
                {
                    if(keyStrings=='id')
                    {
                        customCardTokenId=String.valueOf(customerTokenMap.get(keyStrings));
                        system.debug('id--->'+customerTokenMap.get(keyStrings));
                    }
                }
            }
            Http customCardHTTP =new Http();
            HttpRequest customCardRequest = new HttpRequest();
            customCardRequest.setEndpoint('https://api.stripe.com/v1/customers/'+customCustomerId+'/sources');
            customCardRequest.setHeader('Authorization', 'Bearer ' + 'sk_test_51JuwKmSBDzxwoDrtNY5mR9NtpQ5jHEXY8iRBhZ0KtgraDM9O9yKpbGVNzE9ZNCXXGuFWQbnjjDkI8iIznFVyqzR800y7vqpp9K');
            customCardRequest.setMethod('POST');
            customCardRequest.setBody('source='+customCardTokenId);
            HttpResponse customcardResponse = customHTTP.send(customCardRequest);
            if(customcardResponse.getStatusCode()==200)
            {
                system.debug('success');
            }
        }
        catch(exception e)
        {
            system.debug('error line is-->'+e.getLineNumber());
            system.debug('error message--->'+e.getMessage());
        }
    }	
}