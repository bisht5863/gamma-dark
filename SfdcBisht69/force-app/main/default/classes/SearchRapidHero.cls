public class SearchRapidHero {
    @AuraEnabled
    public static List<String> fun3(String heroName)
    {
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://superhero-search.p.rapidapi.com/api/?hero='+heroName);
        req.setMethod('GET');
        req.setHeader('X-RapidAPI-Host', 'superhero-search.p.rapidapi.com');
        req.setHeader('x-rapidapi-key', 'a96bb3b774msha8fce68ec4c9f20p122798jsn8af91ce825ac');
        HttpResponse responseBody = h.send(req); 
        system.debug('responseBody-->'+responseBody.getBody());
        JSON2Apex customJsonForm =(JSON2Apex) System.JSON.deserialize(responseBody.getBody(), JSON2Apex.class);
        List<String> dataList = new List<String>();
        system.debug('customJsonForm-->'+customJsonForm.images.lg);
        dataList.add(customJsonForm.images.lg);
        dataList.add(customJsonForm.name);
        dataList.add(String.valueOf(customJsonForm.Powerstats.intelligence));
        dataList.add(String.valueOf(customJsonForm.Powerstats.strength));
        dataList.add(String.valueOf(customJsonForm.Powerstats.speed));
        dataList.add(String.valueOf(customJsonForm.Powerstats.durability));
        dataList.add(String.valueOf(customJsonForm.Powerstats.power));
        dataList.add(String.valueOf(customJsonForm.Powerstats.combat));
        dataList.add(customJsonForm.biography.placeOfBirth);
        dataList.add(customJsonForm.biography.firstAppearance);
        dataList.add(customJsonForm.biography.publisher);
        dataList.add(customJsonForm.work.occupation);
        for(String s:dataList)
        {
            system.debug('s-->'+s);
        }
        return dataList;
    }
    public class JSON2Apex {
        public Integer id;
        public String name;
        public String slug;
        public Powerstats powerstats;
        public Biography biography;
        public Work work;
        public Images images;
    }
    public class Images {
        public String xs;
        public String sm;
        public String md;
        public String lg;
    }
    public class Biography {
        public String fullName;
        public String alterEgos;
        public List<String> aliases;
        public String placeOfBirth;
        public String firstAppearance;
        public String publisher;
        public String alignment;
    }
    
    public class Work {
        public String occupation;
        public String base;
    }
    
    public class Powerstats {
        public Integer intelligence;
        public Integer strength;
        public Integer speed;
        public Integer durability;
        public Integer power;
        public Integer combat;
    }
}