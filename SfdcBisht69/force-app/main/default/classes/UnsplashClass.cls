public class UnsplashClass {
    @AuraEnabled
    public static String fun()
    {
        
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.unsplash.com/photos/random?client_id=TvJqs8uLV1s-9hka54bsvwrO8xqIevjmR67QQpWeBBk');
        req.setMethod('GET');
        HttpResponse responseBody = h.send(req);
        JSON2Apex customJsonForm =(JSON2Apex) System.JSON.deserialize(responseBody.getBody(), JSON2Apex.class);
        system.debug('customJsonForm-->'+customJsonForm);
        System.debug('customJsonForm.raw'+customJsonForm.Urls.get('raw'));
           
        system.debug('responce body '+responseBody.getBody()); 
        return customJsonForm.Urls.get('raw');
    }
    @AuraEnabled
    public static String fun2()
    {
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.unsplash.com/photos/random?client_id=TvJqs8uLV1s-9hka54bsvwrO8xqIevjmR67QQpWeBBk');
        req.setMethod('GET');
        HttpResponse responseBody = h.send(req);
        JSON2Apex customJsonForm =(JSON2Apex) System.JSON.deserialize(responseBody.getBody(), JSON2Apex.class);
        system.debug('customJsonForm-->'+customJsonForm);
        System.debug('customJsonForm.raw'+customJsonForm.Urls.get('raw'));
           
        system.debug('responce body '+responseBody.getBody()); 
        return customJsonForm.Urls.get('raw');
    }
    @AuraEnabled
    public static String fun3()
    {
        
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.unsplash.com/photos/random?client_id=TvJqs8uLV1s-9hka54bsvwrO8xqIevjmR67QQpWeBBk');
        req.setMethod('GET');
        HttpResponse responseBody = h.send(req);
        JSON2Apex customJsonForm =(JSON2Apex) System.JSON.deserialize(responseBody.getBody(), JSON2Apex.class);
        system.debug('customJsonForm-->'+customJsonForm);
        System.debug('customJsonForm.raw'+customJsonForm.Urls.get('raw'));
           
        system.debug('responce body '+responseBody.getBody()); 
        return customJsonForm.Urls.get('raw');
    }
    @AuraEnabled
    public static String fun4()
    {
        
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.unsplash.com/photos/random?client_id=TvJqs8uLV1s-9hka54bsvwrO8xqIevjmR67QQpWeBBk');
        req.setMethod('GET');
        HttpResponse responseBody = h.send(req);
        JSON2Apex customJsonForm =(JSON2Apex) System.JSON.deserialize(responseBody.getBody(), JSON2Apex.class);
        system.debug('customJsonForm-->'+customJsonForm);
        System.debug('customJsonForm.raw'+customJsonForm.Urls.get('raw'));
           
        system.debug('responce body '+responseBody.getBody()); 
        return customJsonForm.Urls.get('raw');
    }
    public class JSON2Apex {
        public map<string,string> Urls ;
    }
    public class Urls {
        public String raw;
        public String full;
        public String regular;
        public String small;
        public String thumb;
        public String small_s3;
    }  
}