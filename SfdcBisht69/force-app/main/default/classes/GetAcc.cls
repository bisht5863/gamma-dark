public class GetAcc {
    @AuraEnabled
    public static List<Account> methodName1(String nameInstance){
		return [SELECT Id,Name,website,phone FROM Account WHERE Name LIKE : '%'+nameInstance+'%'];        
    }
    
    @AuraEnabled
    public static void methodName2(List<Account> accList){
        System.debug('accList-->'+accList);
		update accList;       
    }
}