public class YtsClass {
    @AuraEnabled
    public static YtsWrapper fun()
    {
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://yts.mx/api/v2/list_movies.json?sort=seeds&limit=20');
        req.setMethod('GET');
        HttpResponse responseBody = h.send(req);
        YtsWrapper customJsonForm =(YtsWrapper) System.JSON.deserialize(responseBody.getBody(), YtsWrapper.class);
        //  system.debug('responseBody-->'+responseBody.getBody());
        // system.debug('customJsonForm--->'+customJsonForm.data.movies.size());
        // system.debug('movieLink-->'+customJsonForm.data.movies[0].torrents[0].url);
        return customJsonForm;
    }
    @AuraEnabled
    public static YtsWrapper searchMovieFun(String searchMovieVar)
    {
        //   try{
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://yts.mx/api/v2/list_movies.json?query_term='+searchMovieVar);
        req.setMethod('GET');
        HttpResponse responseBody = h.send(req);
        YtsWrapper customJsonForm =(YtsWrapper) System.JSON.deserialize(responseBody.getBody(), YtsWrapper.class);
        //  system.debug('responseBody-->'+responseBody.getBody());
        // system.debug('customJsonForm--->'+customJsonForm.data.movies.size());
        // system.debug('movieLink-->'+customJsonForm.data.movies[0].torrents[0].url);   
        if(customJsonForm.data.movie_count=='0'){
            customJsonForm.status='not find';
            //   system.debug('exception-->'+e);
            system.debug('status-->'+customJsonForm.status);
            return customJsonForm;
        }
        return customJsonForm;
        //}
        
        /* catch(exception e)
{
YtsWrapper customJsonForm= new YtsWrapper();
customJsonForm.status='not find';
system.debug('exception-->'+e);
system.debug('status-->'+customJsonForm.status);
return customJsonForm;
} */
    }
    @AuraEnabled
    public static YtsWrapper clickGenreMovie(String genreMovieVar)
    {
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://yts.mx/api/v2/list_movies.json?genre='+genreMovieVar);
        req.setMethod('GET');
        HttpResponse responseBody = h.send(req);
        YtsWrapper customJsonForm =(YtsWrapper) System.JSON.deserialize(responseBody.getBody(), YtsWrapper.class);
        //  system.debug('responseBody-->'+responseBody.getBody());
        system.debug('customJsonForm--->'+customJsonForm.data.movies.size());
        system.debug('movieLink-->'+customJsonForm.data.movies[0].torrents[0].url);   
        system.debug('movieId-->'+customJsonForm.data.movies[0].id); 
        return customJsonForm;
    }
    public class YtsWrapper
    {
        @AuraEnabled
        public Data data;
        @AuraEnabled
        public String status;
    }
    public class Data
    {
        @AuraEnabled
        public List<Movies> movies;
        @AuraEnabled
        public String movie_count;
    }
    public class Movies
    {
        @AuraEnabled
        public integer id;
        @AuraEnabled
        public String title_long;
        @AuraEnabled
        public Integer year;
        @AuraEnabled
        public double rating;
        @AuraEnabled
        public double runtime;
        // public    List<Genres> genres;
        @AuraEnabled
        public String summary;
        @AuraEnabled
        public String language;
        @AuraEnabled
        public String large_cover_image;
        @AuraEnabled
        public String small_cover_image;
        @AuraEnabled
        public List<Torrents> torrents;
    }
    
    public class Torrents
    {
        @AuraEnabled
        public String url;
        @AuraEnabled
        public String quality;
        @AuraEnabled
        public integer seeds;
        @AuraEnabled
        public integer peers;
        @AuraEnabled
        public String size;
        @AuraEnabled
        public String date_uploaded;
        
    }
    /*  public class Genres
{
public   list<String> genres;
} */
}