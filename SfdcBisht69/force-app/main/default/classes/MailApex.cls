public class MailApex {
    @AuraEnabled(cacheable=true)
    public static void fun()
    {   
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
        semail.setTemplateId('00X5j000000h07QEAQ');
		String[] sendingTo = new String[]{'atul.panwar@cloudanalogy.com'};
		semail.setToAddresses(sendingTo);
		String[] sendingToBccAdd = new String[]{'abhishek.bisht@cloudanalogy.com'};
		semail.setBccAddresses(sendingToBccAdd);
		String[] sendingTocAdd = new String[]{'abhishek.bisht@cloudanalogy.com'};
		semail.setCcAddresses(sendingTocAdd);
		semail.setSubject('Color code with height and width');
		semail.setPlainTextBody('this is body only');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
    }
}