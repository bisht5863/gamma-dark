public class OppLineItemTriggerHandler{
    public static void opportunitylineitemMethod(List<Opportunitylineitem> oppLineList){
        Set<Id> oppIdSet = new Set<Id>();
        for(opportunitylineitem opp : oppLineList){
            oppIdSet.add(opp.opportunityId);
        }
        Map<Id,Double> oppVsOppLineMap = new Map<Id,Double>();
        Map<Id,Double> accVsOppMap = new Map<Id,Double>();
        for(opportunitylineitem opp : [SELECT Id,opportunityId,quantity FROM opportunitylineitem WHERE opportunityId IN : oppIdSet]){
            if(!oppVsOppLineMap.containsKey(opp.opportunityId)){
                oppVsOppLineMap.put(opp.opportunityId,opp.quantity);
            }
            else{
                if(oppVsOppLineMap.get(opp.opportunityId) > opp.quantity){
                    oppVsOppLineMap.put(opp.opportunityId,opp.quantity);
                }
            }
        }
        
        for(opportunity opp : [SELECT Id,AccountId FROM opportunity WHERE Id IN : oppVsOppLineMap.keySet() AND AccountId != NULL]){
            if(!accVsOppMap.containsKey(opp.AccountId)){
                accVsOppMap.put(opp.AccountId,oppVsOppLineMap.get(opp.Id));
            }
            else{
                if(accVsOppMap.get(opp.AccountId) > oppVsOppLineMap.get(opp.Id)){
                    accVsOppMap.put(opp.AccountId,oppVsOppLineMap.get(opp.Id));
                }
            }
        }
        
        
        List<Account> updateAccList = new List<Account>();
        for(Account acc : [SELECT Id,minQuantity__c FROM Account WHERE ID IN : accVsOppMap.keySet()]){
            acc.minQuantity__c = accVsOppMap.get(acc.Id);
            updateAccList.add(acc);
        } 
        update updateAccList;
    }
    
    public static void getMinimumQuantity(List<OpportunityLineItem> oppLineList){
         Set<Id> oppIdSet = new Set<Id>();
        Set<Id> accIdSet = new Set<Id>();
        for(OpportunityLineItem opp : oppLineList){
            oppIdSet.add(opp.OpportunityId);
        }
        for(Opportunity opp : [SELECT Id,AccountId FROM Opportunity WHERE Id IN : oppIdSet]){
            accIdSet.add(opp.AccountId);
        }
        List<OpportunityLineItem> oppLineItemList = [SELECT Id,
                                                    Quantity,
                                                    Product2Id,
                                                    OpportunityId 
                                                    FROM OpportunityLineItem 
                                                    WHERE Opportunity.AccountId IN : accIdSet];
        Map<Id,Map<Id,Double>> map1 = new Map<Id,Map<Id,Double>>();
        Map<Id,Double> map2 = new Map<Id,Double>();
        for(OpportunityLineItem opp : oppLineItemList){
            if(!map1.containsKey(opp.OpportunityId)){
                Map<Id,Double> mapInstance = new Map<Id,Double>();
                mapInstance.put(opp.Product2Id,opp.Quantity);
                map1.put(opp.OpportunityId,mapInstance);          
            }
            else{
                if(!map1.get(opp.OpportunityId).containsKey(opp.Product2Id)){
                    map1.get(opp.OpportunityId).put(opp.Product2Id,opp.Quantity);
                }
                else{
                    map1.get(opp.OpportunityId).put(opp.Product2Id,opp.Quantity + map1.get(opp.OpportunityId).get(opp.Product2Id));
                }
            }
        }
        for(Id opp : map1.keySet()){
            for(Id proInstance : map1.get(opp).keySet()){                
                if(!map2.containsKey(opp)){
                    map2.put(opp,map1.get(opp).get(proInstance));
                }
                else{
                    if(map1.get(opp).get(proInstance) < map2.get(opp)){
                        map2.put(opp,map1.get(opp).get(proInstance));
                    }
                }
            }
        }
        Map<Id,Double> map3 = new Map<Id,Double>();
        List<Account> updateAccList = new List<Account>();
        for(Opportunity opp : [SELECT Id,AccountId FROM Opportunity WHERE Id IN :map2.keySet()]){
            if(!map3.containsKey(opp.AccountId)){
                map3.put(opp.AccountId,map2.get(opp.Id));
            }
            else{
                map3.put(opp.AccountId,map2.get(opp.Id) + map3.get(opp.AccountId));
            }
        }
        for(Account acc : [SELECT Id,Minimum_Quantity__c FROM Account WHERE Id IN : map3.keySet()]){
            acc.Minimum_Quantity__c = map3.get(acc.Id);
            updateAccList.add(acc);
        }
        update updateAccList;
    }
}