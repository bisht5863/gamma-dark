public interface Shape {
    // All other functionality excluded
    Double area();
}