public class B2BCMSContentService {
    @AuraEnabled
        public static ConnectAPi.ManagedContentVersionCollection getContentServiceImage( String cmsContentId){
            String loggedInUserId = UserInfo.getUserId();
            //String subsidiary = CMT_Utilities.getSubsidiaryCodeFromBaseUrl();
            String communityId = Network.getNetworkId();
            //if(Test.isRunningTest()){
            Network pub = [SELECT id FROM Network WHERE Name ='GammaStore' LIMIT 1];
                 communityId = pub.Id;
           // }
            System.debug('communityId -- '+communityId);
            SectionCMSContent sectionCmsContentWrapper = new SectionCMSContent();
            //String cmsContentId = '20Y17000000CdXNEA0';
            //Currently supports News type content only.
            ConnectAPi.ManagedContentVersionCollection cmsContents = B2BCMSContentService.getManagedContentByIds(communityId, cmsContentId, null);
           // System.debug('cmsContents==>>'+cmsContents.Items[0].contentNodes.get('contentKey'));
            return cmsContents;
    }
        /**
         * @description getManagedContentByIds description
         * @param  communityId      Network Id
         * @param  cmsContentId     Id OR key for CMS content item
         * @param  contentType      CMS Content type
         * @return
         */
        public static ConnectAPi.ManagedContentVersionCollection getManagedContentByIds(String communityId, String cmsContentId, String contentType) {
            ConnectAPi.ManagedContentVersionCollection cmsContents = null;
            Boolean isContentId = true;
            try{
                //this checks if cmsContentId is the Id of the cms content item, or if it's a key
                Id contentId = Id.valueOf(cmsContentId);
            } catch(System.StringException stringEx){
                isContentId = false;
            }
            // Fetch CMS Content for that specific community
            if (isContentId) {
                //if we were provided with an Id, use this method
                cmsContents = ConnectApi.ManagedContent.getManagedContentByIds(communityId, new List<String>{cmsContentId}, 0, null, null, contentType, false);
            } else {
                //if not, assume we were provided with a key
                cmsContents = ConnectApi.ManagedContent.getManagedContentByContentKeys(communityId, new List<String>{cmsContentId}, 0, null, null, contentType, false);
            }
            return cmsContents;
        }
        public class SectionCMSContent {
        @AuraEnabled public ConnectApi.ManagedContentMediaNodeValue image;
        @AuraEnabled public String content;
        @AuraEnabled public String communityUrl;
        @AuraEnabled public String title;
        }
    }