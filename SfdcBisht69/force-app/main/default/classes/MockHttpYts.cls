@istest
global class MockHttpYts implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest request){
        HttpResponse response = new HttpResponse();
        //   response.setBody('{"animal": {"id":2, "name":"Test"}}');
        response.setBody('{"data":{"movies":[{"id" : 1 ,"title_long" : "hello","year" : 2002,"rating" : 9.0,"runtime" : 2.1,"summary" : "hello","language" : "Eng","large_cover_image" : "image","small_cover_image" : "image","torrents" : [{"url" : "hello","quality" : "360p","seeds" : 10,"peers" : 1,"size" : "1.2 gb","date_uploaded" : "12/2/2000"}]}]},"status" : "good"}');
            response.setStatusCode(200);
        return response; 
    }
    
}