public class MapApexSession {
    public static void fun(){
        Map<id,double> map1 = new Map<id,double>();
        List<OpportunityLineItem> opplineList = new List<OpportunityLineItem>();
        opplineList = [select id,quantity,opportunityId from OpportunityLineItem];
        set<id>oppID = new set<id>();
        for(OpportunityLineItem oppline : opplineList){
            oppID.add(oppline.OpportunityId);
            if(!map1.containsKey(oppline.OpportunityId)){
                map1.put(oppline.OpportunityId,oppline.Quantity);
            }
            else{
                double quan = map1.get(oppline.OpportunityId);
                map1.put(oppline.OpportunityId,quan+oppline.Quantity);
            }
        }
        List<Opportunity> oppList = new List<Opportunity>();
        oppList = [select id,AccountId from Opportunity where AccountId!=null and id IN:oppID];
        Map<id,double> accMap = new Map<id,double>();
        set<id>accId = new set<id>();
        for(Opportunity opp : oppList){
            accId.add(opp.AccountId);
            if(!accMap.containsKey(opp.AccountId)){
                accMap.put(opp.AccountId,map1.get(opp.id));
            }
            else{
                accMap.put(opp.AccountId,map1.get(opp.id)+accMap.get(opp.AccountId));
            }
        }
        list<Account> accList = new List<Account>();
        list<Account> accListNew = new List<Account>();
        accList= [select id,name,OpportunityQuantity__c from Account where id In:accId];
        for(Account acc:accList){
            system.debug('acc name-->'+acc+' quantity--> '+accMap.get(acc.id));
            acc.OpportunityQuantity__c=accMap.get(acc.id);
            accListNew.add(acc);
        }
        update accListNew;
    }
}