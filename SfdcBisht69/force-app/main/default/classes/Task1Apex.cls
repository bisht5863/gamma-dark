public class Task1Apex {
    @AuraEnabled
    public static void fun(String oppName,Date oppDate,String oppStageName,String oppDescription,String proName,Double proQuantity,Integer proPrice)
    {
        Opportunity opp = new Opportunity();
        opp.Name=oppName;
        opp.StageName=oppStageName;
        opp.CloseDate=oppDate;
        opp.Description=oppDescription;
        insert opp;
        System.debug('proid-->'+proName+' '+'oppId-->'+opp);
        Product2 pro = new Product2();
        pro.Name=proName;
        insert pro;
        Pricebook2 prcBook = new Pricebook2();
        prcBook = [Select id from Pricebook2 where IsStandard=True limit 1];
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id=prcBook.ID;
        pbe.Product2Id=pro.Id;
        pbe.UnitPrice=1;
        pbe.IsActive=true;
        insert pbe;
        System.debug('pbe--->'+pbe);
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId=opp.ID;
        oli.PricebookEntryId=pbe.ID;
        oli.UnitPrice=proPrice;
        oli.Quantity=proQuantity;
        insert oli;
        System.debug('oli--->'+oli);
    }
}