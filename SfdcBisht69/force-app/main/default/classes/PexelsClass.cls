public class PexelsClass {
    @AuraEnabled
	public static List<String>  fun(String imageName)
    {
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.pexels.com/v1/search?query='+imageName+'&page=1&per_page=10');
        req.setMethod('GET');
        req.setHeader('Authorization', '563492ad6f91700001000001b82a61b4f2364758bd8b1795b4945d12');
        HttpResponse responseBody = h.send(req);
        JSON2Apex customJsonForm =(JSON2Apex) System.JSON.deserialize(responseBody.getBody(), JSON2Apex.class);
        system.debug('reponsebody-->'+customJsonForm.photos[0].src.original);
        List<String> imageList = new List<String>();
        for(integer x=0;x<10;x++)
        {
            imageList.add(customJsonForm.photos[x].src.original);
            system.debug('imageList-->'+customJsonForm.photos[x].src.original);
        }
        return imageList;
    }
    public class JSON2Apex {
        public Integer page;
	public Integer per_page;
	public List<Photos> photos;
	public Integer total_results;
	public String next_page;
    }
    public class Photos {
		public Integer id;
		public Integer width;
		public Integer height;
		public String url;
		public String photographer;
		public String photographer_url;
		public Integer photographer_id;
		public String avg_color;
		public Src src;
		public Boolean liked;
		public String alt;
	}
    public class Src {
		public String original;
		public String large2x;
		public String large;
		public String medium;
		public String small;
		public String portrait;
		public String landscape;
		public String tiny;
	}
}