public class ProcessApprovelRequest {
    /**
* This method will submit the opportunity for approval
**/
    public static void submitForApproval(Opportunity opp){        
        // Create an approval request for  Opportunity
        system.debug('inside submitForApproval');        
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();          
        req.setComments('Submitting approval request using Trigger');        
        req.setObjectId(opp.id);
        // Submit the approval request for the Opportunity        
        Approval.ProcessResult result = Approval.process(req);        
    }     
    
    /**
*Get ProcessInstanceWorkItemId using SOQL
**/
    public static Id getWorkItemId(Id targetObjectId){
        system.debug('inside getWorkItemId');
        Id workItemId = null;
        for(ProcessInstanceWorkitem workItem  : [Select p.Id from ProcessInstanceWorkitem p where p.ProcessInstance.TargetObjectId =: targetObjectId]){
            workItemId  =  workItem.Id;
        }
        return workItemId;
    }
    
    /**
* This method will Approve the opportunity
**/
    public static void approveRecord(Opportunity opp){
        system.debug('inside approveRecord');
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Approving request using Trigger');
        req.setAction('Approve');        
        Id workItemId = getWorkItemId(opp.id); 
        if(workItemId == null){
            opp.addError('Error Occured in Trigger');
        }
        else{
            req.setWorkitemId(workItemId);
            // Submit the request for approval
            Approval.ProcessResult result =  Approval.process(req);
        }
    }
    
    /**
* This method will Reject the opportunity
**/
    public static void rejectRecord(Opportunity opp){
        system.debug('inside rejectRecord');
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Rejected request using Trigger');
        req.setAction('Reject');
        Id workItemId = getWorkItemId(opp.id);
        if(workItemId == null){
            opp.addError('Error Occured in Trigger');
        }
        else{
            req.setWorkitemId(workItemId);
            // Submit the request for approval
            Approval.ProcessResult result =  Approval.process(req);
        }
    }
}