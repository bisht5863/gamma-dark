public class  YtsMail {
    @AuraEnabled(cacheable=true)
    public static void mailFun(List<String> mailList)
    {
        system.debug('mailList-->'+mailList);
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
        String[] sendingTo = new String[]{'abhishek.bisht@cloudanalogy.com'};
            semail.setToAddresses(sendingTo);
        semail.setSubject(mailList[1]);
        // mailList[3]=mailList[3].replaceAll('<[/a-zAZ0-9]*>','');
        system.debug('mailList[3]-->'+mailList[3]);
        semail.setPlainTextBody('Email:-'+mailList[2]+' Name:-'+mailList[0]+' '+mailList[3]);
        semail.setHTMLBody('Email:-'+mailList[2]+' Name:-'+mailList[0]+'<br> '+mailList[3]);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail}); 
    }
}