public class VoteSuper {
    @AuraEnabled
    public static List<VoteWrapper> votingFunction(){
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://akabab.github.io/superhero-api/api/all.json');
        req.setMethod('GET');
        HttpResponse responseBody = h.send(req);
        system.debug('response body-->'+responseBody.getBody());
        List<VoteWrapper> customJsonForm =(List<VoteWrapper>) System.JSON.deserialize(responseBody.getBody(), List<VoteWrapper>.class);
        // system.debug('customJsonForm-->'+customJsonForm[0].name);
       
        return customJsonForm;
    }
    public class VoteWrapper{
        @AuraEnabled
        public Integer id;
        @AuraEnabled
        public String name;
        @AuraEnabled
        public Image images;
    }
    public class Image{
        @AuraEnabled
        public String lg;
    }
}