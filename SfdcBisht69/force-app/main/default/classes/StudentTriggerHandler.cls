public class StudentTriggerHandler {
    public static void updateTotalSubjectRecord(List<Student__c> studentList){
        set<id> setId = new set<Id>();
        for(Student__c s : studentList){
            setId.add(s.SubjectId__c);
        }
        List<aggregateresult> aggInstance = [SELECT count(id) ct,SubjectId__r.Name n  FROM Student__c WHERE SubjectId__c IN : setId group by SubjectId__r.Name];
        system.debug('aggInstance-->'+aggInstance.size());
        map<string,double> map3 = new map<string,double>();
        for(aggregateresult agg : aggInstance){
            if(!map3.containsKey((String)agg.get('n'))){
                 map3.put((String)agg.get('n'),1);
            }
            else{
                map3.put((String)agg.get('n'),map3.get((String)agg.get('n')) +1);
            }
           // map3.put((String)agg.get('n'),(double)agg.get('ct'));
        }
     system.debug('map3-->'+map3);
       
        List<Subject__c> subList= [SELECT Id,name from Subject__c WHERE id IN : setId];
        set<String> nameSet = new set<String>();
        for(Subject__c s : subList){
            nameSet.add(s.Name);
        }
       
        List<Total_Student__c> totalStudentList = [SELECT Id,Count__c,Subject__c,Name FROM Total_Student__c WHERE Subject__c IN : nameSet];
        System.debug('totalStudentList-->'+totalStudentList);
        List<Total_Student__c> updateTotalStudentList = new List<Total_Student__c>();
        Map<String,String> map1 = new Map<String,String>();
        Map<String,Double> map2 = new Map<String,Double>();
        for(Total_Student__c totalInstance : totalStudentList){
            map1.put(totalInstance.Subject__c,totalInstance.Name);
            
            if(map3.containsKey(totalInstance.Subject__c)){
                map2.put(totalInstance.Subject__c,map3.get(totalInstance.Subject__c));
            }
        }
        System.debug('map1-->'+map1);
        System.debug('map2-->'+map2);
        for(Subject__c studentInstance : subList){
            if(map1.containskey(studentInstance.Name)){
                System.debug('here');
                Total_Student__c totalInstance = new Total_Student__c();
                totalInstance.Count__c = map3.get(studentInstance.Name);
                totalInstance.Subject__c = map1.get(studentInstance.Name);
                totalInstance.Name = map1.get(studentInstance.Name);
                updateTotalStudentList.add(totalInstance);
            }
        }
        upsert updateTotalStudentList Subject__c;
        System.debug('updateTotalStudentList-->'+updateTotalStudentList);
    }
}