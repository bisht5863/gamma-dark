public class ApproveRequestClass {
    public static void fun(){
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
req.setObjectId('0065j00000alQ3fAAE');
//If the next step in your approval process is another Apex approval process, you specify exactly one user ID as the next approver. 
//If not, you cannot specify a user ID and this method must be null.
id ide ='0055j000005DGagAAG';
req.setNextApproverIds(new Id[] {'0055j000005DGagAAG'});
Approval.ProcessResult processResult = Approval.process(req);
//Valid values are: Approved, Rejected, Removed or Pending.
System.assertEquals('Pending', processResult.getInstanceStatus());
    }
}