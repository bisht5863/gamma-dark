public class NewsClass {
    @AuraEnabled
    public static list<JSON2Apex> fun()
    {
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://khabar.p.rapidapi.com/hindi');
        req.setMethod('GET');
        req.setHeader('X-RapidAPI-Host', 'khabar.p.rapidapi.com');
        req.setHeader('x-rapidapi-key', 'a96bb3b774msha8fce68ec4c9f20p122798jsn8af91ce825ac');
        HttpResponse responseBody = h.send(req);
        list<JSON2Apex> customJsonForm =(list<JSON2Apex>) System.JSON.deserialize(responseBody.getBody(), list<JSON2Apex>.class);
        system.debug('body-->'+responseBody.getBody());
        system.debug('size-->'+customJsonForm.size());
        return customJsonForm;
    }
    public class JSON2Apex {
        @AuraEnabled
        public String title;
        @AuraEnabled
        public String source;
    }
}