public class GetCustomOpportunityList {
    @AuraEnabled(Cacheable=true)
    public static list<Opportunity> getOpportunityList()
    {
        List<Opportunity> oppList = new List<Opportunity>();
        oppList = [SELECT ID,Name,Account.Name,Account.Website,StageName,CloseDate,OwnerId from Opportunity];
        System.debug('oppList-->'+oppList);
        return oppList;
    }
}