public class MapClass1 {
    public static void methodName(){
        map<Id,Integer> mapInstance = new map<Id,Integer>();
        List<Opportunity> oppList = new List<Opportunity>();
        oppList = [Select id,AccountId FROM Opportunity where AccountId!=Null];
        Set<Id> accSet = new Set<Id>();
        
        
        for(Opportunity oppInstance : oppList){
            accSet.add(oppInstance.AccountId);
            if(!mapInstance.containsKey(oppInstance.AccountId)){
                mapInstance.put(oppInstance.AccountId,1);
            }
            else{
                mapInstance.put(oppInstance.AccountId,mapInstance.get(oppInstance.AccountId)+1);
            }
        }
        
        for(Id InstanceId: mapInstance.keyset()){
            system.debug('Account Id-->'+InstanceId+' Number of Opportunity-->'+mapInstance.get(InstanceId));
        }
    }
}