public class ResultSuperClass {
    @AuraEnabled
    public static void resultMethod(String winner,String loser){
        system.debug('winner-->'+winner+' loser-->'+loser);
        SuperSuper__c winnerInstance = [SELECT Id,
                                        Name,
                                        TotalRound__c,
                                        Voting_Percentage__c,
                                        WinningRound__c 
                                        FROM SuperSuper__c WHERE Name=: winner limit 1];
        winnerInstance.TotalRound__c = winnerInstance.TotalRound__c + 1;
        winnerInstance.WinningRound__c = winnerInstance.WinningRound__c + 1;
        winnerInstance.Voting_Percentage__c = (winnerInstance.WinningRound__c/winnerInstance.TotalRound__c ) * 100;
        update winnerInstance;
        
        SuperSuper__c loserInstance = [SELECT Id,
                                       Name,
                                       TotalRound__c,
                                       Voting_Percentage__c,
                                       WinningRound__c
                                       FROM SuperSuper__c WHERE Name=: loser limit 1];
        loserInstance.TotalRound__c = loserInstance.TotalRound__c + 1;
        loserInstance.WinningRound__c = loserInstance.WinningRound__c - 1;
        loserInstance.Voting_Percentage__c = (loserInstance.WinningRound__c/loserInstance.TotalRound__c ) * 100;
        update loserInstance;
        system.debug('winner-->'+winnerInstance.Name);
        system.debug('loser-->'+loserInstance.Name);
    }
}