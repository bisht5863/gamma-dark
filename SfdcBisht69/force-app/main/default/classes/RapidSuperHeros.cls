public class RapidSuperHeros {
    @AuraEnabled
    public static Wrapper fun()
    {
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://superhero-search.p.rapidapi.com/api/heroes');
        req.setMethod('GET');
        req.setHeader('X-RapidAPI-Host', 'superhero-search.p.rapidapi.com');
        req.setHeader('x-rapidapi-key', 'a96bb3b774msha8fce68ec4c9f20p122798jsn8af91ce825ac');
        HttpResponse responseBody = h.send(req);
        list<JSON2Apex> customJsonForm =(list<JSON2Apex>) System.JSON.deserialize(responseBody.getBody(), list<JSON2Apex>.class);
        list<Object> results = (list<Object>) JSON.deserializeUntyped(responseBody.getBody());
        Wrapper wr = new Wrapper();
        list<String>nameString=new list<String>();
        list<String>imagesString=new list<String>();
        list<String>publisherString=new list<String>();
        list<String>placeOfBirthString=new list<String>();
        list<String>occupationString=new list<String>();
        for(integer x=0;x<customJsonForm.size();x++)
        {
            system.debug('heroName->'+customJsonForm[x].name);
            nameString.add(customJsonForm[x].name);
            imagesString.add(customJsonForm[x].images.lg);
            publisherString.add(customJsonForm[x].biography.publisher);
            placeOfBirthString.add(customJsonForm[x].biography.placeOfBirth);
            occupationString.add(customJsonForm[x].work.occupation);
        }
        wr.nameList=nameString;
        wr.imageList=imagesString;
        wr.publisherList=publisherString;
        wr.placeOfBirthList=placeOfBirthString;
        wr.occupationList=occupationString;
        system.debug('responseBody--->'+responseBody.getBody());
        return wr;
    }
    public class JSON2Apex {
        public Integer id;
        public String name;
        public String slug;
        public Powerstats powerstats;
        public Appearance appearance;
        public Biography biography;
        public Work work;
        public Connections connections;
        public Images images; 
    }
    public class Connections {
        public String groupAffiliation;
        public String relatives;
    }
    public class Images {
        public String xs;
        public String sm;
        public String md;
        public String lg;
    }
    public class Biography {
        public String fullName;
        public String alterEgos;
        public List<String> aliases;
        public String placeOfBirth;
        public String firstAppearance;
        public String publisher;
        public String alignment;
    }
    public class Work {
        public String occupation;
        public String base;
    }
    public class Powerstats {
        public Integer intelligence;
        public Integer strength;
        public Integer speed;
        public Integer durability;
        public Integer power;
        public Integer combat;
    }
    
    public class Appearance_Z {
        public String gender;
        public Object race;
        public List<String> height;
        public List<String> weight;
        public String eyeColor;
        public String hairColor;
    }
    
    public class Appearance {
        public String gender;
        public String race;
        public List<String> height;
        public List<String> weight;
        public String eyeColor;
        public String hairColor;
    } 
    public class Wrapper{
        @AuraEnabled
        public List<String> nameList;
        @AuraEnabled
        public List<String> imageList;
        @AuraEnabled
        public List<String> publisherList;
        @AuraEnabled
        public List<String> placeOfBirthList;
        @AuraEnabled
        public List<String> occupationList;
    }
    @AuraEnabled
    public static Wrapper fun2()
    {
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://superhero-search.p.rapidapi.com/api/villains');
        req.setMethod('GET');
        req.setHeader('X-RapidAPI-Host', 'superhero-search.p.rapidapi.com');
        req.setHeader('x-rapidapi-key', 'a96bb3b774msha8fce68ec4c9f20p122798jsn8af91ce825ac');
        HttpResponse responseBody = h.send(req);
        list<JSON2Apex> customJsonForm =(list<JSON2Apex>) System.JSON.deserialize(responseBody.getBody(), list<JSON2Apex>.class);
        list<Object> results = (list<Object>) JSON.deserializeUntyped(responseBody.getBody());
        Wrapper wr = new Wrapper();
        list<String>nameString=new list<String>();
        list<String>imagesString=new list<String>();
        list<String>publisherString=new list<String>();
        list<String>placeOfBirthString=new list<String>();
        list<String>occupationString=new list<String>();
        for(integer x=0;x<customJsonForm.size();x++)
        {
            system.debug('heroName->'+customJsonForm[x].name);
            nameString.add(customJsonForm[x].name);
            imagesString.add(customJsonForm[x].images.lg);
            publisherString.add(customJsonForm[x].biography.publisher);
            placeOfBirthString.add(customJsonForm[x].biography.placeOfBirth);
            occupationString.add(customJsonForm[x].work.occupation);
        }
        wr.nameList=nameString;
        wr.imageList=imagesString;
        wr.publisherList=publisherString;
        wr.placeOfBirthList=placeOfBirthString;
        wr.occupationList=occupationString;
        system.debug('responseBody--->'+responseBody.getBody());
        return wr;
    }
}