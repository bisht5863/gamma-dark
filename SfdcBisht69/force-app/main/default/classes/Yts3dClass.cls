public class Yts3dClass {
    @AuraEnabled
    public static YtsWrapper fun1(String qualityVar)
    {
        Http h =new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://yts.mx/api/v2/list_movies.json?quality='+qualityVar);
        req.setMethod('GET');
        HttpResponse responseBody = h.send(req);
        YtsWrapper customJsonForm =(YtsWrapper) System.JSON.deserialize(responseBody.getBody(), YtsWrapper.class);
        //  system.debug('responseBody-->'+responseBody.getBody());
        system.debug('customJsonForm--->'+customJsonForm.data.movies.size());
        system.debug('movieLink-->'+customJsonForm.data.movies[0].torrents[0].url);
        return customJsonForm;
    }
    public class YtsWrapper
    {
        @AuraEnabled
        public Data data;
        @AuraEnabled
        public String status;
    }
    public class Data
    {
        @AuraEnabled
        public List<Movies> movies;
    }
    public class Movies
    {
        @AuraEnabled
        public String title_long;
        @AuraEnabled
        public Integer year;
        @AuraEnabled
        public double rating;
        @AuraEnabled
        public double runtime;
        // public    List<Genres> genres;
        @AuraEnabled
        public String summary;
        @AuraEnabled
        public String language;
        @AuraEnabled
        public String large_cover_image;
        @AuraEnabled
        public List<Torrents> torrents;
    }
    
    public class Torrents
    {
        @AuraEnabled
        public String url;
        @AuraEnabled
        public String quality;
        @AuraEnabled
        public integer seeds;
        @AuraEnabled
        public integer peers;
        @AuraEnabled
        public String size;
        @AuraEnabled
        public String date_uploaded;
    }
    /*  public class Genres
{
public   list<String> genres;
} */
}